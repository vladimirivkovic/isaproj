/*global angular*/
var app = angular.module('app', ['app.controllers', 'app.services', 'ngRoute', 'ui.bootstrap']);

app.config(function ($routeProvider, $locationProvider) {
    "use strict";
    $routeProvider
        .when('/home', {
            templateUrl: 'partials/home.html',
            controller: 'HomeCtrl',
        })
        .when('/register', {
            templateUrl: 'partials/register.html',
            controller: 'RegisterCtrl',
        })
        .when('/login', {
            templateUrl: 'partials/login.html',
            controller: 'LoginCtrl',
            resolve: {
                title: function () { return "Login"; },
            }
        })
        .when('/logout', {
            template: '',
            controller: 'LogoutCtrl'
        })
        .when('/reserve1', {
            templateUrl: 'partials/reserve1.html',
            controller: 'ReserveCtrl'
        })
        .when('/reserve2', {
            templateUrl: 'partials/reserve2.html',
            controller: 'ReserveCtrl'
        }) 
        .when('/reserve3', {
            templateUrl: 'partials/reserve3.html',
            controller: 'ReserveCtrl'
        })
        .when('/invite/:invitationId', {
            templateUrl: 'partials/invitation.html',
            controller: 'InviteCtrl'
        }) 
        .when('/restaurant/:restaurantId', {
            templateUrl: 'partials/restaurant.html',
            controller: 'MenuCtrl'
        })  
        .otherwise({
            redirectTo: '/home'
        });
    
});

app.run(function ($rootScope) {
    "use strict";
    $rootScope.display = 'login';
});
