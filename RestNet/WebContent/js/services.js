/*global angular*/
angular.module('app.services', [
	'app.Restaurants',
	'app.Managers',
	'app.Guests',
	'app.Reservations',
	'app.Visits',
]);