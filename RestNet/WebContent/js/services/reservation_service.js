/*global angular*/
var appReservationModule = angular.module('app.Reservations', []);

appReservationModule.factory('Reservations', function ($http) {
    'use strict';
    
    return {
        getGuests : function () {
            return $http.get('rest/guests');
        },
        reserve : function (data) {
            return $http({
                method: 'POST',
                url: 'rest/reserve/',
                data: data
            })
        },
        getReserved : function (data) {
            return $http({
                method: 'POST',
                url: 'rest/reserve/check',
                data: data
            })
        },
        invite : function (data) {
            return $http({
                method: 'POST',
                url: 'rest/invite',
                data: data
            })
        },
        inviteInfo : function (data) {
            return $http.get('rest/invite/info/' + data);
        },
        respond : function (data) {
            return $http({
                method: 'POST',
                url: 'rest/invite/respond',
                data: data
            })
        },
    };
});