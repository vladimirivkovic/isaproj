/*global angular*/
var appRestaurantsModule = angular.module('app.Restaurants', []);

appRestaurantsModule.factory('Restaurants', function ($http) {
    'use strict';
    
    return {
        getRestaurants : function () {
            return $http.get('rest/restaurants');
        },
    	getRestaurant : function(data) {
    		return $http.get('rest/restaurants/' + data);
    	},
        getMenu : function(data) {
            return $http.get('rest/restaurants/' + data + '/menu');
        },
    	addRestaurant : function(data) {
    		return $http({
                method: 'POST',
                url: 'rest/restaurants/',
                data: data
            });
    	},
        editRestaurant : function(data, id) {
            return $http({
                method: 'PUT',
                url: 'rest/restaurants/' + id,
                data: data
            });
        },
        addFood : function(data, id) {
            return $http({
                method: 'POST',
                url: 'rest/restaurants/' + id + '/menu',
                data: data
            });
        },
        editFood : function(data, id, fid) {
            return $http({
                method: 'PUT',
                url: 'rest/restaurants/' + id + '/menu/' + fid,
                data: data
            });
        },
        removeFood : function (id, fid) {
            return $http({
                method: 'DELETE',
                url : 'rest/restaurants/' + id + '/menu/' + fid
            });
        },
        configTables : function(id, data) {
            return $http({
                method: 'POST',
                url : 'rest/restaurants/' + id + '/tables',
                data : data
            });
        },
        getTables : function (id) {
            return $http.get('rest/restaurants/' + id + '/tables');
        },

        reserve : function(data) {
            return $http({
                method: 'POST',
                url : 'rest/reserve/',
                data : data
            })
        },
    };
});