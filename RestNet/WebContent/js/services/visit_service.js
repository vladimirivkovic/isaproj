/*global angular*/
var appVisitModule = angular.module('app.Visits', []);

appVisitModule.factory('Visits', function ($http) {
    'use strict';
    
    return {
        getVisits : function () {
            return $http.get('rest/visits');
        },
        rateVisit : function (data, id) {
            return $http({
                method : 'POST',
                url : 'rest/visits/' + id,
                data : data
            })
        },
    };
});