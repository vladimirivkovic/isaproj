/*global angular*/
var appGuestsModule = angular.module('app.Guests', []);

appGuestsModule.factory('Guests', function ($http) {
    'use strict';
    
    return {
        getGuests : function () {
            return $http.get('rest/guests');
        },
        getGuest : function (id) {
            return $http.get('rest/guests/' + id);
        },
        registerGuest : function (data) {
            return $http({
                method: 'POST',
                url: 'rest/guests/',
                data: data
            });
        },
        updateGuest : function (data, id) {
            return $http({
                method: 'PUT',
                url: 'rest/guests/' + id,
                data: data
            });
        },
        getFriends : function (id) {
            return $http.get('rest/guests/' + id + '/friends');
        },
        getNotFriends : function (id) {
            return $http.get('rest/guests/' + id + '/notfriends');
        },
        addFriend : function (id, fid) {
            return $http({
                method: 'POST',
                url: 'rest/guests/' + id + '/friends',
                data: fid
            });
        },
        removeFriend : function (id, fid) {
            return $http({
                method: 'DELETE',
                url: 'rest/guests/' + id + '/friends/' + fid,
                data: fid
            });
        },
    };
});