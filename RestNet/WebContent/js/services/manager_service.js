/*global angular*/
var appManagersModule = angular.module('app.Managers', []);

appManagersModule.factory('Managers', function ($http) {
    'use strict';
    
    return {
        getManagers : function () {
            return $http.get('rest/managers');
        },
    	getManager : function(data) {
    		return $http.get('rest/managers/' + data);
    	},
    	addManager : function(data) {
    		return $http({
                method: 'POST',
                url: 'rest/managers/',
                data: data
            });
    	}
    };
});