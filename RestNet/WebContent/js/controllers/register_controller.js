/*global angular*/
/*global $*/
var appRegisterCtrlModule = angular.module('app.RegisterCtrl', []);

appRegisterCtrlModule.controller('RegisterCtrl', function($rootScope, $scope,
		$location, $http, $httpParamSerializer, Guests) {
	"use strict";
	
	$scope.alertMessage = null;

	$('#name_input').focus();

	$scope.register = function() {
		if (!$scope.name) {
			$scope.alertMessage = 'Name field can\'t be empty!';
			return;
		}
		if (!$scope.surname) {
			$scope.alertMessage = 'Surname field can\'t be empty!';
			return;
		}
		if (!$scope.address) {
			$scope.alertMessage = 'Address field can\'t be empty!';
			return;
		}
		if (!$scope.username) {
			$scope.alertMessage = 'Username field can\'t be empty!';
			return;
		}

		if (!$scope.password) {
			$scope.alertMessage = 'Password field can\'t be empty!';
			return;
		}

		if (!$scope.rpassword) {
			$scope.alertMessage = 'Repeated password field can\'t be empty!';
			return;
		}

		if ($scope.password != $scope.rpassword) {
			$scope.alertMessage = 'Passwords don\'t match!';
			return;
		}

		Guests.registerGuest({
			guestDTO : {
				username : $scope.username,
				password : $scope.password,
				name : $scope.name,
				surname : $scope.surname,
				address : $scope.address,
			}
		})
		.success(function(data, status) {
			$rootScope.role = data.role;
			$location.path('/');
		}).error(function(data, status) {
			$scope.alertMessage = "Status code: " + status;
		});
	};

});