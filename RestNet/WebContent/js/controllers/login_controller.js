/*global angular*/
/*global $*/
var appLoginCtrlModule = angular.module('app.LoginCtrl', []);

appLoginCtrlModule.controller('LoginCtrl', function($rootScope, $scope,
		$location, $http, $httpParamSerializer, title) {
	"use strict";

	$scope.title = title;
	$scope.alertMessage = null;

	$('#username_input').focus();

	$rootScope.loginStatus(null);

	$scope.login = function() {
		if (!$scope.username) {
			$scope.alertMessage = 'Username field can\'t be empty!';
			return;
		}

		if (!$scope.password) {
			$scope.alertMessage = 'Password field can\'t be empty!';
			return;
		}

		$http({
			method : 'POST',
			url : 'rest/auth/login',
			data : {
				'credentials' : {
					'username' : $scope.username,
					'password' : $scope.password
				}
			}
		}).success(function(data, status) {
			$location.path('#/home');
		}).error(function(data, status) {
			$scope.alertMessage = "Status code: " + status;
		});
	};

});