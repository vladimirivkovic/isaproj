/*global angular*/
var appHomeCtrlModule = angular.module('app.HomeCtrl', []);

appHomeCtrlModule.controller('HomeCtrl', function ($rootScope, $scope, $http, $location, $uibModal, $window, Restaurants, Managers, Guests, Reservations, Visits) {
    "use strict";

  $scope.predicate = '';
  $scope.reverse = true;
  $scope.order = function(predicate) {
    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
    $scope.predicate = predicate;
  };

    // admin
    $scope.restaurants = {}
    $scope.managers = {}

    // manager
    $scope.restaurant = {}
    $scope.menu = {}

    // guest
    $scope.tablesInRest = null;
    $scope.friends = {}
    $scope.notfriends = {}
    $scope.guests = {}
    $scope.unrated = 0;

    

    $scope.deg2rad = function (deg) {
        return deg * (Math.PI/180)
    }

    $scope.getDistanceFromLatLonInKm = function (lat1,lon1,lat2,lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = $scope.deg2rad(lat2-lat1);  // deg2rad below
        var dLon = $scope.deg2rad(lon2-lon1); 
        var a = 
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos($scope.deg2rad(lat1)) * Math.cos($scope.deg2rad(lat2)) * 
                Math.sin(dLon/2) * Math.sin(dLon/2); 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c; // Distance in km
        return Math.round(d * 100) / 100;
    }

    $scope.calculateDistances = function() {
        if ($window.navigator.geolocation) {
            $window.navigator.geolocation.getCurrentPosition(function(position){
                $scope.$apply(function(){
                    $scope.position = position;
                    console.log(position.coords);
                    for(var i in $scope.restaurants) {
                        $scope.restaurants[i].distance = $scope.getDistanceFromLatLonInKm(
                            $scope.position.coords.latitude,
                            $scope.position.coords.longitude,
                            $scope.restaurants[i].latitude,
                            $scope.restaurants[i].longitude
                            );
                    }
                });
            });
        }
    }



    

    $scope.init = function() {
        if ($rootScope.role == 'A') {
            Restaurants.getRestaurants().success(function (data) {
                $scope.restaurants = $scope.get1list(data.restaurantList[0].restaurants);
            });

            Managers.getManagers().success(function (data) {
                $scope.managers = $scope.get1list(data.managerList[0].managers);
            });
        }
        if ($rootScope.role == 'M') {
            Restaurants.getRestaurant($rootScope.restId).success(function (data) {
                $scope.restaurant = data.restaurant[0];
            });

            Restaurants.getMenu($rootScope.restId).success(function (data) {
                $scope.menu = $scope.get1list(data.menu[0].menu);
            });

            Restaurants.getTables($rootScope.restId).success(function (data) {
                $scope.tablesInRest = $scope.get1list(data.tableList[0].tables);
            
                    $scope.tables = {width: 7, height: 7, content: [], saved: $scope.tablesInRest != null};
                    for (var i in _.range($scope.tables.width)) {
                        var x = [];
                        for (var j in _.range($scope.tables.height)) {
                            x.push("x");
                        }
                        $scope.tables.content.push(x);
                    }
                    
                    if($scope.tablesInRest)
                    for (var i in $scope.tablesInRest) {
                        $scope.tables.content[$scope.tablesInRest[i].x][$scope.tablesInRest[i].y]  = $scope.tablesInRest[i].name;
                    }

                $scope.tables.rows = _.range($scope.tables.width);
                $scope.tables.columns = _.range($scope.tables.height);

            });
        }
        if ($rootScope.role == 'G') {
            $scope.friend_name = {}
            $scope.friend_name.name = '';
            $scope.friend_name.surname = '';

            Restaurants.getRestaurants().success(function (data) {
                $scope.restaurants = $scope.get1list(data.restaurantRatingList[0].restaurants);
                $scope.calculateDistances();
            });

            Guests.getFriends($rootScope.id).success(function (data) {
                $scope.friends = $scope.get1list(data.friendList[0].friends);
            });

            Guests.getGuests().success(function (data) {
                $scope.guests = $scope.get1list(data.friendList[0].friends);
            });

            Guests.getNotFriends($rootScope.id).success(function (data) {
                $scope.notfriends = $scope.get1list(data.friendList[0].friends);
            });

            Visits.getVisits().success(function (data) {
                $scope.visits = $scope.get1list(data.visitList[0].visits);

                $scope.unrated = 0;
                for(var x in $scope.visits) {
                    if(!$scope.visits[x].rated) {
                        $scope.unrated++;
                    }
                    $scope.visits[x].dateX = (new Date($scope.visits[x].reservation.date1)).toDateString();
                    $scope.visits[x].friends = $scope.get1list($scope.visits[x].friends);
                };
            });

            Guests.getGuest($rootScope.id).success(function (data) {
                $scope.guest = data.guestOutDTO[0];
            });
        }
    };

    $rootScope.loginStatus($scope.init);

    // ADMIN

    $scope.addRestaurant = function() {
        var modal = $uibModal.open({
            animation: true,
            templateUrl: 'partials/modals/add_restaurant.html',
            controller: 'AddRestaurantCtrl',
            size: 'lg',
        });
         
        modal.result.then(function (refresh) {
            if (refresh) {
                $scope.init();
            }
        });
    };

    $scope.addManager = function() {
        var modal = $uibModal.open({
            animation: true,
            templateUrl: 'partials/modals/add_manager.html',
            controller: 'AddManagerCtrl',
            size: 'lg',
            resolve: {
                restaurants: function() {
                    return $scope.restaurants;
                }
            }
        });
         
        modal.result.then(function (refresh) {
            if (refresh) {
                $scope.init();
            }
        });
    };

    // MANAGER

    $scope.editRestaurant = function() {
        var modal = $uibModal.open({
            animation: true,
            templateUrl: 'partials/modals/add_restaurant.html',
            controller: 'EditRestaurantCtrl',
            size: 'lg',
            resolve: {
                restaurant: function() {
                    return $scope.restaurant;
                }
            }
        });
         
        modal.result.then(function (refresh) {
            if (refresh) {
                $scope.init();
            }
        });
    };

    

    $scope.addFood = function() {
        var modal = $uibModal.open({
            animation: true,
            templateUrl: 'partials/modals/add_food.html',
            controller: 'AddFoodCtrl',
            size: 'lg',
            resolve: {
                restaurant: function() {
                    return $scope.restaurant;
                }
            }
        });
         
        modal.result.then(function (refresh) {
            if (refresh) {
                $scope.init();
            }
        });
    };

    $scope.editFood = function(food) {
        var modal = $uibModal.open({
            animation: true,
            templateUrl: 'partials/modals/add_food.html',
            controller: 'EditFoodCtrl',
            size: 'lg',
            resolve: {
                restaurant: function() {
                    return $scope.restaurant;
                },
                food: function() {
                    return food;
                }
            }
        });
         
        modal.result.then(function (refresh) {
            if (refresh) {
                $scope.init();
            }
        });
    };

    $scope.removeFood = function(id) {
        Restaurants.removeFood($rootScope.restId, id).success(function (data) {
            $scope.init();
        }).error(function (data, status) {
            alert(status);
        });
    };

    $scope.saveConfig = function() {
        var tables = [];
        for (var i = 0; i < $scope.tables.width; i++) {
            for (var j = 0; j < $scope.tables.height; j++) {
                console.log($scope.tables.content[i][j]);
                if ($scope.tables.content[i][j] != '' && $scope.tables.content[i][j] != 'x') {
                    tables.push({
                        name : $scope.tables.content[i][j],
                        x : i,
                        y : j,
                    });

                    console.log('evo ga ' + $scope.tables.content[i][j] + ' na ' + i + ' ' + j);
                }
            }
        }

        var data = {
            'tableList' : {
                'tables' : tables
            }
        };
        Restaurants.configTables($scope.restaurant.id, data).success(function (data, status) {
            $scope.init();
        }).error(function (data, status) {
            alert(status);
        });
    };

    // GUEST

    $scope.addFriend = function(fid) {
        Guests.addFriend($rootScope.id, fid).success(function (data, status) {
            $scope.init();
        }).error(function (data, status) {
        alert(status);
        });
    };
    $scope.removeFriend = function(fid) {
        Guests.removeFriend($rootScope.id, fid).success(function (data, status) {
            $scope.init();
        }).error(function (data, status) {
        alert(status);
        });
    };

    $scope.startReservation = function(rid) {
        Reservations.restaurant = {};
        Reservations.restaurant.id = rid;
        $location.path('/reserve1');
    };

    $scope.rateVisit = function(id, rate) {
        if(!rate) {
            alert("Please choose positive number of stars");
            return;
        }

        var data = {
            ratingString : rate
        };

        Visits.rateVisit(rate, id).success(function (data) {
            Visits.getVisits().success(function (data) {
                $scope.init();
            });
        }).error(function (data, status) {
            alert(status);
        });
    };

    $scope.editAccount = function() {
        var modal = $uibModal.open({
            animation: true,
            templateUrl: 'partials/modals/edit_account.html',
            controller: 'EditAccountCtrl',
            size: 'lg',
            resolve: {
                guest: function() {
                    return $scope.guest;
                },
            }
        });
         
        modal.result.then(function (refresh) {
            if (refresh) {
                $scope.init();
            }
        });
    };
});