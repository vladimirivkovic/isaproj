/*global angular*/
var appIndexCtrlModule = angular.module('app.IndexCtrl', []);

appIndexCtrlModule.controller('IndexCtrl', function ($rootScope, $scope, $http, $location) {
    "use strict";

    $rootScope.loginStatus = function(callback) {
        $http.get('rest/auth/status')
            .success(function (data, status) {
                $rootScope.role = data.statusResponse[0].role;
                $rootScope.restId = data.statusResponse[0].restId;
                $rootScope.id = data.statusResponse[0].id;

                if (callback) callback();
            }).error(function (data, status) {
            	$rootScope.id = null;
            	$location.path('/login');
    		});;
    };

    $rootScope.get1list = function(data) {
        if( Object.prototype.toString.call(data) === '[object Array]' ) {
            return data;
        } else if (typeof data === "undefined") {
            return null;
        } else {
            return [ data ];
        }
    }

    //$scope.loginStatus();
});