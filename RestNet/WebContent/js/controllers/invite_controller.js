/*global angular*/
/*global $*/
var appInviteCtrlModule = angular.module('app.InviteCtrl', []);

appInviteCtrlModule.controller('InviteCtrl', function($rootScope, $scope,
		$location, $http, $routeParams, $httpParamSerializer, Guests, Restaurants, Reservations) {
	"use strict";

	$scope.reservation = {};

	$scope.invitationId = $routeParams.invitationId;

	$scope.date1 = {};
	$scope.date2 = {};

	$scope.friend = {};

	$rootScope.loginStatus($scope.init);

	$scope.init = function () {
		Reservations.inviteInfo($scope.invitationId).success(function (data) {
			$scope.reservation = data.invitationDTO[0].reservation;
			$scope.friend = data.invitationDTO[0].inviteFrom;
			$scope.date1 = (new Date(data.invitationDTO[0].reservation.date1)).toString();;
			$scope.date2 = (new Date(data.invitationDTO[0].reservation.date2)).toString();;
		}).error(function (data, status) {
			alert("Invitation expired or do not exists! Error: " + status);
			$location.path("/");
		});
	};

	$scope.init();

	$scope.accept = function (answer) {
		var data = {
			invitationAnswer : {
				invitationId : $scope.invitationId,
				ans : answer
			}
		}

		Reservations.respond(data).success(function (data) {
			$location.path("/");
		}).error(function (data, status) {
			alert(status);
			$location.path("/");
		});
	}
});