var appEditFoodCtrlModule = angular.module('app.EditFoodCtrl', []);

appEditFoodCtrlModule.controller('EditFoodCtrl', function($rootScope,
		$scope, $uibModalInstance, Restaurants, restaurant, food) {
	"use strict";

	$scope.title = "Edit food";
	$scope.name = food.name;
	$scope.description = food.description;
	$scope.price = food.price;

	$scope.alertMessage = null;

	$scope.save = function() {
		if (!$scope.name) {
			$scope.alertMessage = 'Name field can\'t be empty!';
			return;
		}

		if (!$scope.description) {
			$scope.alertMessage = 'description field can\'t be empty!';
			return;
		}

		if (!$scope.price) {
			$scope.alertMessage = 'price field can\'t be empty!';
			return;
		}

		var price = parseFloat($scope.price);
		if(isNaN(price) || price < 0) {
			$scope.alertMessage = 'Latitude must be positive number!';
			return;
		}

		var data = {
			foodDTO : {
				name : $scope.name,
				description : $scope.description,
				price : $scope.price,
			}
		};

		Restaurants.editFood(data, restaurant.id, food.id).success(function(data, status) {
			$uibModalInstance.close(true);
		}).error(function(data, status) {
			$scope.alertMessage = "Error: " + status;
		});
	};

	$scope.close = function() {
		$uibModalInstance.close(false);
	};

});