/*global angular*/
var appAddManagerCtrlModule = angular.module('app.AddManagerCtrl', []);

appAddManagerCtrlModule.controller('AddManagerCtrl', function($rootScope,
		$scope, $uibModalInstance, Managers, restaurants) {
	"use strict";

	$scope.title = "Add new manager";
	$scope.restaurants = restaurants;

	$scope.alertMessage = null;

	$scope.save = function() {
		if (!$scope.username) {
			$scope.alertMessage = 'Username field can\'t be empty!';
			return;
		}

		if (!$scope.password) {
			$scope.alertMessage = 'Password field can\'t be empty!';
			return;
		}

		if (!$scope.rpassword) {
			$scope.alertMessage = 'Repeated password field can\'t be empty!';
			return;
		}

		if (!$scope.restaurant || $scope.restaurant == "") {
			$scope.alertMessage = 'Restaurant field can\'t be empty!';
			return;
		}

		if (!($scope.password == $scope.rpassword)) {
			$scope.alertMessage = 'Passwords do not match!';
			return;
		}

		var data = {
			managerDTO : {
				username : $scope.username,
				password : $scope.password,
				restaurant : $scope.restaurant,
			}
		};

		Managers.addManager(data).success(function(data, status) {
			$uibModalInstance.close(true);
		}).error(function(data, status) {
			$scope.alertMessage = "Error: " + status;
		});
	};

	$scope.close = function() {
		$uibModalInstance.close(false);
	};

});