var appEditAccountCtrlModule = angular.module('app.EditAccountCtrl', []);

appEditAccountCtrlModule.controller('EditAccountCtrl', function($rootScope,
		$scope, $uibModalInstance, guest, Guests) {
	"use strict";

	$scope.name = guest.name;
	$scope.surname = guest.surname;
	$scope.username = guest.username;
	$scope.oldPassword = '';
	$scope.newPassword = '';
	$scope.rnewPassword = '';
	$scope.address = guest.address;

	$scope.alertMessage = null;

	$scope.save = function() {
		if (!$scope.name) {
			$scope.alertMessage = 'Name field can\'t be empty!';
			return;
		}
		if (!$scope.surname) {
			$scope.alertMessage = 'Surname field can\'t be empty!';
			return;
		}
		if (!$scope.address) {
			$scope.alertMessage = 'Address field can\'t be empty!';
			return;
		}
		if (!$scope.username) {
			$scope.alertMessage = 'Username field can\'t be empty!';
			return;
		}

		if (!$scope.oldPassword) {
			$scope.alertMessage = 'Old password field can\'t be empty!';
			return;
		}

		if (!$scope.newPassword) {
			$scope.alertMessage = 'New password field can\'t be empty!';
			return;
		}

		if (!$scope.rnewPassword) {
			$scope.alertMessage = 'Repeated password field can\'t be empty!';
			return;
		}

		if ($scope.rnewPassword != $scope.newPassword) {
			$scope.alertMessage = 'Passwords don\'t match!';
			return;
		}

		var data = {
			guestDTO : {
				username : $scope.username,
				password : $scope.oldPassword,
				newPassword : $scope.newPassword,
				name : $scope.name,
				surname : $scope.surname,
				address : $scope.address,
			}
		};

		Guests.updateGuest(data, guest.id).success(function(data, status) {
			$uibModalInstance.close(true);
		}).error(function(data, status) {
			$scope.alertMessage = "Error: " + status;
		});
	};

	$scope.close = function() {
		$uibModalInstance.close(false);
	};

});