/*global angular*/
var appEditRestaurantCtrlModule = angular.module('app.EditRestaurantCtrl', []);

appAddRestaurantCtrlModule.controller('EditRestaurantCtrl', function($rootScope,
		$scope, $uibModalInstance, Restaurants, restaurant) {
	"use strict";

	$scope.title = "Edit restaurant";
	$scope.name = restaurant.name;
	$scope.type = restaurant.type;
	$scope.longitude = restaurant.longitude;
	$scope.latitude = restaurant.latitude;

	$scope.alertMessage = null;

	$scope.save = function() {
		if (!$scope.name) {
			$scope.alertMessage = 'Name field can\'t be empty!';
			return;
		}

		if (!$scope.type) {
			$scope.alertMessage = 'Address field can\'t be empty!';
			return;
		}

		if (!$scope.longitude) {
			$scope.alertMessage = 'longitude field can\'t be empty!';
			return;
		}

		if (!$scope.latitude) {
			$scope.alertMessage = 'latitude field can\'t be empty!';
			return;
		}

		var data = {
			restaurantDTO : {
				name : $scope.name,
				type : $scope.type,
				longitude : $scope.longitude,
				latitude : $scope.latitude
			}
		};

		Restaurants.editRestaurant(data, restaurant.id).success(function(data, status) {
			$uibModalInstance.close(true);
		}).error(function(data, status) {
			$scope.alertMessage = "Error: " + status;
		});
	};

	$scope.close = function() {
		$uibModalInstance.close(false);
	};

});