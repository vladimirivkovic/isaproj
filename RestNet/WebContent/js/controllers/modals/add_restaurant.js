/*global angular*/
var appAddRestaurantCtrlModule = angular.module('app.AddRestaurantCtrl', []);

appAddRestaurantCtrlModule.controller('AddRestaurantCtrl', function($rootScope,
		$scope, $uibModalInstance, Restaurants) {
	"use strict";

	$scope.title = "Add new restaurant";

	$scope.alertMessage = null;

	$scope.save = function() {
		if (!$scope.name) {
			$scope.alertMessage = 'Name field can\'t be empty!';
			return;
		}

		if (!$scope.type) {
			$scope.alertMessage = 'Address field can\'t be empty!';
			return;
		}

		if (!$scope.longitude) {
			$scope.alertMessage = 'Longitude field can\'t be empty!';
			return;
		}

		if (!$scope.latitude) {
			$scope.alertMessage = 'Latitude field can\'t be empty!';
			return;
		}

		var lat = parseFloat($scope.latitude);
		if(isNaN(lat) || lat < -90 || lat > 90) {
			$scope.alertMessage = 'Latitude must be number between -90 and 90!';
			return;
		}

		var lon = parseFloat($scope.longitude);
		if(isNaN(lon) || lon < -90 || lon > 90) {
			$scope.alertMessage = 'Longitude must be number between -180 and 180!';
			return;
		}

		var data = {
			restaurantDTO : {
				name : $scope.name,
				type : $scope.type,
				longitude : $scope.longitude,
				latitude : $scope.latitude
			}
		};

		Restaurants.addRestaurant(data).success(function(data, status) {
			$uibModalInstance.close(true);
		}).error(function(data, status) {
			$scope.alertMessage = "Error: " + status;
		});
	};

	$scope.close = function() {
		$uibModalInstance.close(false);
	};

});