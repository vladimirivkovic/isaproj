/*global angular*/
/*global $*/
var appReserveCtrlModule = angular.module('app.ReserveCtrl', []);

appReserveCtrlModule.controller('ReserveCtrl', function($rootScope, $scope,
		$location, $http, $httpParamSerializer, Guests, Restaurants, Reservations) {
	"use strict";

	$scope.loginStatus();

	$scope.restaurant = {}

	$scope.tablesInRest = {}

	$scope.duration = 1;
	$scope.mytime = new Date(0);

	$scope.friends = {}

	$scope.init = function() {
		if(!Reservations.restaurant) {
			$location.path("/");
		}

		Restaurants.getRestaurant(Reservations.restaurant.id).success(function (data) {
			$scope.restaurant = data.restaurant[0];
		});

		if (Reservations.date) {
			$scope.date = Reservations.date;
			$scope.mytime = Reservations.time;
			$scope.duration = Reservations.duration;
		

			Restaurants.getTables(Reservations.restaurant.id).success(function (data) {
	            $scope.tablesInRest = $scope.get1list(data.tableList[0].tables);
	        
	                $scope.tables = {width: 7, height: 7, content: [], selected : [], saved: $scope.tablesInRest != null};
	                for (var i in _.range($scope.tables.width)) {
	                    var x = [];
	                    var sel = [];
	                    for (var j in _.range($scope.tables.height)) {
	                        x.push({name : "x"});
	                        sel.push(false);
	                    }
	                    $scope.tables.content.push(x);
	                    $scope.tables.selected.push(sel);
	                }
	                
	                for (var i in $scope.tablesInRest) {
	                    $scope.tables.content[$scope.tablesInRest[i].x][$scope.tablesInRest[i].y]  = $scope.tablesInRest[i];
	                }

	            $scope.tables.rows = _.range($scope.tables.width);
	            $scope.tables.columns = _.range($scope.tables.height);

	            $scope.getReserved();
	        });

			Guests.getFriends($scope.id).success(function (data) {
	             $scope.friends = $scope.get1list(data.friendList[0].friends);
	        });

		}
	}

	$rootScope.loginStatus($scope.init);

	$scope.today = function() {
    	$scope.dt = new Date();
    	$scope.dt.setHours(0);
		$scope.dt.setMinutes(0);
		$scope.dt.setSeconds(0);
		$scope.dt.setMilliseconds(0);
	};
	$scope.today();

	$scope.clear = function() {
		$scope.dt = null;
	};

	$scope.toggleMin = function() {
		$scope.minDate = $scope.minDate ? null : new Date();
	};

	$scope.toggleMin();
	$scope.maxDate = new Date(2020, 5, 22);

	$scope.open1 = function() {
		$scope.popup1.opened = true;
	};

	$scope.setDate = function(year, month, day) {
		$scope.dt = new Date(year, month, day);
	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1
	};

	$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	$scope.format = $scope.formats[0];
	$scope.altInputFormats = ['M!/d!/yyyy'];

	$scope.popup1 = {
		opened: false
	};

	var tomorrow = new Date();

	tomorrow.setDate(tomorrow.getDate() + 1);
	var afterTomorrow = new Date();
	afterTomorrow.setDate(tomorrow.getDate() + 1);
	$scope.events =
	[
	  {
	    date: tomorrow,
	    status: 'full'
	  },
	  {
	    date: afterTomorrow,
	    status: 'partially'
	  }
	];

	$scope.getDayClass = function(date, mode) {
		if (mode === 'day') {
		  var dayToCheck = new Date(date).setHours(0,0,0,0);

		  for (var i = 0; i < $scope.events.length; i++) {
		    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

		    if (dayToCheck === currentDay) {
		      return $scope.events[i].status;
		    }
		  }
		}

		return '';
	};

		
	

	$scope.reserve = function() {

	};

	$scope.reserveTables = function() {
		Reservations.date = $scope.dt;
		Reservations.time = $scope.mytime;
		Reservations.duration = $scope.duration;

		$location.path("/reserve2");
	}

	$scope.getReserved = function() {
		console.log($scope.date);
		console.log($scope.mytime);
		var data = {
  			"reservationDTO": 
    		{
      			"restaurantId": $scope.restaurant.id,
      			"guestId": $rootScope.id,
      			"date1": $scope.date.getTime() + $scope.mytime.getUTCHours()*(1000*60*60),
      			"date2": $scope.date.getTime() + $scope.mytime.getUTCHours()*(1000*60*60) + $scope.duration*(1000*60*60),
      			"tableIds" : []
    		}
		}

		Reservations.getReserved(data).success(function (data) {
			var reservedTables = $rootScope.get1list(data.reservationDTO[0].tableIds);
			for (var z in reservedTables) {
				for (var i in $scope.tablesInRest) {
                    if ($scope.tables.content[$scope.tablesInRest[i].x][$scope.tablesInRest[i].y].id == reservedTables[z])
                    	$scope.tables.content[$scope.tablesInRest[i].x][$scope.tablesInRest[i].y].reserved = true;
                }
			}
		})
	}

	$scope.inviteFriends = function() {
		var data = {
  			reservationDTO: 
    		{
      			restaurantId: $scope.restaurant.id,
      			guestId: $rootScope.id,
      			date1: $scope.date.getTime() + $scope.mytime.getHours()*(1000*60*60),
      			date2: $scope.date.getTime() + $scope.mytime.getHours()*(1000*60*60) + $scope.duration*(1000*60*60),
      			tableIds : []
    		}
		}

		for (var i = 0; i < $scope.tables.width; i++) {
            for (var j = 0; j < $scope.tables.height; j++) { 
            	if ($scope.tables.selected[i][j]) {
            		console.log("table '" + $scope.tables.content[i][j].id + "' selected");
            		data.reservationDTO.tableIds.push($scope.tables.content[i][j].id);
            	}
            }
        }

        if (data.reservationDTO.tableIds.length) {
	        Reservations.reserve(data).success(function (data, status) {
	        	if (status == 200) {
		        	Reservations.reservationId = data.id;
					$location.path("/reserve3");
				} else {
					alert(status);
				}
			}).error(function (data, status) {
				alert("Be sure that date is correct and then try again");
			})
		} else {
			alert("No table selected!");
		}
	}

	$scope.finish = function () {
		var data = {
  			invitationList: 
    		{
      			reservationId: Reservations.reservationId,
      			guestIds: [],
    		}
		}

		for(var i in $scope.friends) {
			if ($scope.friends[i].selected) {
				console.log($scope.friends[i].name + " invited");
				data.invitationList.guestIds.push($scope.friends[i].id);
			}
		}

		if(data.invitationList.guestIds.length) {
			Reservations.invite(data).success(function (data) {
				$location.path("/");
			}).error(function (data, status) {
				alert(status);
			})
		}

		$location.path("/");
	}

});