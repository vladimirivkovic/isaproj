/*global angular*/
var appLogoutCtrlModule = angular.module('app.LogoutCtrl', []);

appLogoutCtrlModule.controller('LogoutCtrl', function($rootScope, $scope,
		$location, $http, $httpParamSerializer) {
	"use strict";

	$scope.logout = function() {
		$http({
			method : 'POST',
			url : 'rest/auth/logout'
		}).success(function(data, status, headers, config) {
			$rootScope.id = null;
			$rootScope.role = null;
			$rootScope.restId = null;
			$location.path('/login');
		}).error(function(data, status, headers, config) {
			$rootScope.id = null;
			$rootScope.role = null;
			$rootScope.restId = null;
			$location.path('/login');
		});
	};

	$scope.logout();

});