/*global angular*/
/*global $*/
var appMenuCtrlModule = angular.module('app.MenuCtrl', ['ngMap']);

appMenuCtrlModule.controller('MenuCtrl', function($rootScope, $scope,
		$location, $http, $routeParams, Restaurants, Reservations) {
	"use strict";

	$scope.restaurant = {};
	$scope.restaurantId = $routeParams.restaurantId;
	$scope.menu = {}


	$scope.loginStatus($scope.init);

	$scope.init = function () {
		Restaurants.getRestaurant($scope.restaurantId).success(function (data) {
			$scope.restaurant = data.restaurant[0];
		}).error(function (data, status) {
			alert(status);
		});

		Restaurants.getMenu($scope.restaurantId).success(function (data) {
			$scope.menu = $rootScope.get1list(data.menu[0].menu);
		}).error(function (data, status) {
			alert(status);
		});
	};

	$scope.init();

	$scope.startReservation = function(rid) {
        Reservations.restaurant = {};
        Reservations.restaurant.id = rid;
        $location.path('/reserve1');
    };
});