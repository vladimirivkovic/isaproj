angular.module('app.controllers', [
    'app.IndexCtrl',
    'app.HomeCtrl',
    'app.LoginCtrl',
    'app.LogoutCtrl',
    'app.RegisterCtrl',

    'app.AddRestaurantCtrl',
    'app.EditRestaurantCtrl',
    'app.AddManagerCtrl',

    'app.AddFoodCtrl',
    'app.EditFoodCtrl',

    'app.ReserveCtrl',
    'app.InviteCtrl',
    'app.MenuCtrl',
    'app.EditAccountCtrl',

    
]);