package dao;

import java.io.Serializable;
import java.util.Date;

import dto.ReservationDTO;
import entity.Guest;
import entity.Reservation;
import entity.ReservedTable;
import entity.TableX;

public interface ReservedTableDaoLocal extends GenericDaoLocal<ReservedTable, Serializable> {
	public boolean isTableReserved(TableX t, Date date1, Date date2);
	public Reservation reserve(Guest g, ReservationDTO reservationDTO);
	public ReservationDTO checkReserve(Guest g, ReservationDTO reservationDTO);
}
