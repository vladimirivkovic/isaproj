package dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import dto.InvitationAnswer;
import dto.InvitationList;
import entity.Guest;
import entity.Invitation;
import entity.Reservation;
import entity.Visit;
import mdb.SendLocal;

@Stateless
@Local(InvitationDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class InvitationDaoBean extends GenericDaoBean<Invitation, Serializable> implements InvitationDaoLocal {

	@EJB
	ReservationDaoLocal rsvdl;

	@EJB
	GuestDaoLocal gdl;

	@EJB
	SendLocal sl;

	@EJB
	VisitDaoLocal vdl;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public void inviteFriends(InvitationList invitationList) {
		Reservation rsv = rsvdl.findById(invitationList.getReservationId());

		if (rsv != null) {
			HashSet<Integer> invited = new HashSet<>();
			for (Integer i : invitationList.getGuestIds()) {
				if(invited.contains(i)) {
					continue;
				}
				Guest g = gdl.findById(i);
				System.out.println("Zove se " + g.getName());
				if (g != null) {
					Invitation inv = new Invitation();

					inv.setGuest(g);
					inv.setReservation(rsv);
					inv.setResponded(false);

					inv = persist(inv);
					
					invited.add(i);

					// send message
					sl.sendInvitation(inv.getId().toString(), g.getUsername());
				}
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Guest getRespond(Invitation invitation) {
		Guest g = invitation.getGuest();

		if (g.getToken() == null) {
			String token = UserDaoBean.issueToken(g.getUsername());
			g.setToken(token);

			g = gdl.merge(g);
		}

		return g;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public boolean respond(InvitationAnswer answer, Guest guest) {
		Invitation inv = findById(answer.getInvitationId());

		if (inv == null) {
			return false;
		}
		System.out.println(new Date());
		if (inv.getReservation().getDate1().after(new Date()) || inv.getResponded().booleanValue() == false) {
			if (inv.getGuest().getId().equals(guest.getId())) {
				inv.setResponded(true);
				inv = merge(inv);

				if (answer.getAns().booleanValue() == true) {
					// create visit
					Visit v = new Visit(inv);
					vdl.persist(v);
				}
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

}
