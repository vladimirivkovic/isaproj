package dao;

import java.io.Serializable;

import entity.Friendship;

public interface FriendshipDaoLocal extends GenericDaoLocal<Friendship, Serializable>{
	public void makeFriends(Integer id1, Integer id2);
	public void removeFriends(Integer id1, Integer id2);
}
