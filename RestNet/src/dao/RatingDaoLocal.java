package dao;

import java.io.Serializable;

import dto.RestaurantRatingList;
import entity.Guest;
import entity.Rating;
import entity.Restaurant;

public interface RatingDaoLocal extends GenericDaoLocal<Rating, Serializable>{
	public Rating getFriendsRating(Guest g, Restaurant r);
	public void setRating(Integer rating, Guest g, Integer id);
	public RestaurantRatingList getListWithRatings(Guest g);
}
