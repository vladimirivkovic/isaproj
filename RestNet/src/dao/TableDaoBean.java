package dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.TypedQuery;

import dto.TableDTO;
import dto.TableList;
import entity.Restaurant;
import entity.TableX;

@Stateless
@Local(TableDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TableDaoBean extends GenericDaoBean<TableX, Serializable> implements TableDaoLocal {
	
	@EJB
	RestaurantDaoLocal rdl;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public List<TableX> getTableIn(Restaurant r) {
		TypedQuery<TableX> q = em.createNamedQuery("getTablesIn", TableX.class);
		q.setParameter("rest", r);
		
		return q.getResultList();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public void configTables(Restaurant r, TableList tableList) {
		if (r.getConfigured().booleanValue() == false) {

			System.out.println(tableList.tables.size());

			for (TableDTO t : tableList.tables) {
				System.out.println(t.getName());
				TableX tx = t.convertToTable();
				tx.setId(null);

				tx.setRestaurant(r);
				persist(tx);
			}

			r.setConfigured(true);
			rdl.merge(r);
		}
	}

}
