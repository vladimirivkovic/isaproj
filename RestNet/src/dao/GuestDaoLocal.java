package dao;

import java.io.Serializable;
import java.util.List;

import dto.FriendList;
import dto.GuestDTO;
import entity.Guest;

public interface GuestDaoLocal extends GenericDaoLocal<Guest, Serializable> {
	public FriendList getFriendsFor(Integer id);
	public List<Guest> getFriendsOfMineFor(Integer id);
	public FriendList getNotFriendsFor(Integer id);

	public Guest register(GuestDTO dto);
	public Guest activate(String token);
}
