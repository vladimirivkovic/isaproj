package dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

import dto.ReservationDTO;
import entity.Guest;
import entity.Reservation;
import entity.ReservedTable;
import entity.Restaurant;
import entity.TableX;
import entity.Visit;

@Stateless
@Local(ReservedTableDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ReservedTableDaoBean extends GenericDaoBean<ReservedTable, Serializable> implements ReservedTableDaoLocal {

	@EJB
	TableDaoLocal tdl;

	@EJB
	ReservationDaoLocal rsvdl;

	@EJB
	RestaurantDaoLocal rstdl;

	@EJB
	VisitDaoLocal vdl;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public boolean isTableReserved(TableX t, Date date1, Date date2) {
		TypedQuery<ReservedTable> q = em.createNamedQuery("isTableReserved", ReservedTable.class);
		q.setParameter("t", t.getId());
		q.setParameter("date1", date1);
		q.setParameter("date2", date2);

		return q.getResultList().size() > 0;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Reservation reserve(Guest g, ReservationDTO reservationDTO) {
		if((new Date()).after(new Date(reservationDTO.getDate1()))) {
			return null;
		}
		
		Restaurant rst = rstdl.findById(reservationDTO.getRestaurantId());

		reservationDTO.removeDuplicates();

		List<TableX> reservedTables = new ArrayList<>();
		for (Integer i : reservationDTO.getTableIds()) {
			TableX t = tdl.findById(i);
			if (t.getRestaurant().equals(rst))
				reservedTables.add(t);

			if (isTableReserved(t, new Date(reservationDTO.getDate1()), new Date(reservationDTO.getDate2()))) {
				return null;
			}
		}

		// LOCK TABLES HERE
		for (TableX t : reservedTables) {
			em.lock(t, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
		}

		Reservation rsv = null;
		try {
			if (reservedTables.size() > 0) {
				rsv = new Reservation();
				rsv.setRestaurant(rst);
				rsv.setDate1(new Date(reservationDTO.getDate1()));
				rsv.setDate2(new Date(reservationDTO.getDate2()));
				rsv.setGuest(g);

				rsv = rsvdl.persist(rsv);

				//Thread.sleep(2000);

				for (TableX t : reservedTables) {
					ReservedTable rt = new ReservedTable();
					rt.setReservation(rsv);
					rt.setTable(t);
					//Thread.sleep(1000);
					persist(rt);
				}
				//Thread.sleep(2000);

				Visit v = new Visit();
				v.setReservation(rsv);
				v.setGuest(g);
				v.setRated(false);
				vdl.persist(v);
			}
		} catch (Exception e) {
			System.out.println("rollbacked");
			e.printStackTrace();
			return null;
		}

		return rsv;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public ReservationDTO checkReserve(Guest g, ReservationDTO reservationDTO) {
		Restaurant rst = rstdl.findById(reservationDTO.getRestaurantId());

		if (rst == null) {
			return null;
		}

		reservationDTO.getTableIds().clear();

		List<TableX> allTables = tdl.getTableIn(rst);
		for (TableX t : allTables) {
			if (isTableReserved(t, new Date(reservationDTO.getDate1()), new Date(reservationDTO.getDate2()))) {
				reservationDTO.getTableIds().add(t.getId());
			}
		}
		return reservationDTO;
	}

}
