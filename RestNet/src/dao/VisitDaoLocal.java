package dao;

import java.io.Serializable;
import java.util.List;

import dto.VisitDTO;
import entity.Guest;
import entity.Visit;

public interface VisitDaoLocal extends GenericDaoLocal<Visit, Serializable> {
	public List<VisitDTO> findVisitsFor(Guest q);
	public int getNumberOfVisits(Guest g);
}
