package dao;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.TypedQuery;

import entity.Friendship;
import entity.Guest;

@Stateless
@Local(FriendshipDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FriendshipDaoBean extends GenericDaoBean<Friendship, Serializable> implements FriendshipDaoLocal {

	@EJB
	GuestDaoLocal gdl;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public void makeFriends(Integer id1, Integer id2) {
		Guest g1 = gdl.findById(id1);
		Guest g2 = gdl.findById(id2);
		
		if (g1 == null || g2 == null) {
			return;
		}
		
		try {
			TypedQuery<Friendship> q = em.createNamedQuery("areFriends", Friendship.class);
			q.setParameter("first", g1);
			q.setParameter("second", g2);
			
			q.getSingleResult();
			return;
		} catch (Exception e) {
			Friendship fship = new Friendship();
			fship.setFirstFriend(g1);
			fship.setSecondFriend(g2);

			persist(fship);
		}
	}

	@Override
	public void removeFriends(Integer id1, Integer id2) {
		Guest g1 = gdl.findById(id1);
		Guest g2 = gdl.findById(id2);
		
		if (g1 == null || g2 == null) {
			return;
		}
		
		try {
			TypedQuery<Friendship> q = em.createNamedQuery("areFriends", Friendship.class);
			q.setParameter("first", g1);
			q.setParameter("second", g2);
			
			Friendship fship = q.getSingleResult();
			remove(fship);
			return;
		} catch (Exception e) {
			return;
		}
	}

}
