package dao;

import java.io.Serializable;

import dto.InvitationAnswer;
import dto.InvitationList;
import entity.Guest;
import entity.Invitation;


public interface InvitationDaoLocal extends GenericDaoLocal<Invitation, Serializable>{
	public void inviteFriends(InvitationList invitationList);
	public Guest getRespond(Invitation invitation);
	public boolean respond(InvitationAnswer invitationAnswer, Guest guest);
}
