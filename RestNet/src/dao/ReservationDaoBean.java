package dao;

import java.io.Serializable;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import entity.Reservation;

@Stateless
@Local(ReservationDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ReservationDaoBean extends GenericDaoBean<Reservation, Serializable> implements ReservationDaoLocal {

}
