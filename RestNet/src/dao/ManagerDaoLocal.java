package dao;

import java.io.Serializable;

import dto.ManagerDTO;
import dto.ManagerList;
import entity.Manager;

public interface ManagerDaoLocal extends GenericDaoLocal<Manager, Serializable> {
	public ManagerList getList();
	public Manager addManager(ManagerDTO managerDTO);
}
