package dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.TypedQuery;

import dto.VisitDTO;
import entity.Guest;
import entity.Visit;

@Stateless
@Local(VisitDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class VisitDaoBean extends GenericDaoBean<Visit, Serializable> implements VisitDaoLocal {

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public List<VisitDTO> findVisitsFor(Guest g) {
		TypedQuery<Visit> q = em.createNamedQuery("findVisitsFor", Visit.class);
		q.setParameter("g", g);

		List<Visit> visits = q.getResultList();
		List<VisitDTO> visitList = new ArrayList<>();
		for (Visit v : visits) {
			VisitDTO dto = new VisitDTO(v);

			TypedQuery<Guest> qx = em.createNamedQuery("findFriendsForVisit", Guest.class);
			qx.setParameter("res", v.getReservation());
			List<Guest> friends = qx.getResultList();

			//System.out.println(v.getReservation().getDate1());
			for (Guest f : friends) {
				if (!f.equals(g)) {
					//System.out.println(f.getName());
					dto.getFriends().add(f.getName());
				}
			}

			visitList.add(dto);
		}

		return visitList;
	}

	@Override
	public int getNumberOfVisits(Guest g) {
		TypedQuery<Visit> q = em.createNamedQuery("findVisitsFor", Visit.class);
		q.setParameter("g", g);

		return q.getResultList().size();
	}

}
