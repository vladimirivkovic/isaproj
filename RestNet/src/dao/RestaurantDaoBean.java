package dao;

import java.io.Serializable;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import dto.RestaurantDTO;
import dto.RestaurantList;
import entity.Restaurant;

@Stateless
@Local(RestaurantDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RestaurantDaoBean extends GenericDaoBean<Restaurant, Serializable> implements RestaurantDaoLocal {
	

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public RestaurantList getList() {
		return new RestaurantList(findAll());
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Restaurant addRestaurant(RestaurantDTO dto) {
		return persist(dto.convertToRestaurant());
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Restaurant updateRestaurant(RestaurantDTO dto, Restaurant r) {
		r = dto.updateRestaurant(r);
		return merge(r);
	}
}
