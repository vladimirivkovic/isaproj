package dao;

import java.io.Serializable;
import java.util.List;

import dto.TableList;
import entity.Restaurant;
import entity.TableX;

public interface TableDaoLocal extends GenericDaoLocal<TableX, Serializable> {
	public List<TableX> getTableIn(Restaurant r);
	public void configTables(Restaurant r, TableList tableList);
}
