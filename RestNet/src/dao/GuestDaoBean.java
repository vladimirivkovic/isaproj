package dao;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.TypedQuery;

import dto.FriendList;
import dto.GuestDTO;
import dto.GuestOutDTO;
import entity.Guest;
import entity.User;

@Stateless
@Local(GuestDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class GuestDaoBean extends GenericDaoBean<Guest, Serializable> implements GuestDaoLocal {

	public static String emailPattern = "^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\\.[a-zA-Z](-?[a-zA-Z0-9])*)+$";
	public static Pattern pattern = Pattern.compile(emailPattern);

	@EJB
	UserDaoLocal udl;
	
	@EJB
	VisitDaoLocal vdl;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public FriendList getFriendsFor(Integer id) {
		User u = udl.findById(id);
		Guest g = (Guest) u;

		if (g == null) {
			return null;
		}

		TypedQuery<Guest> q = em.createNamedQuery("getFriendsFor", Guest.class);
		q.setParameter("guest", g);

		List<Guest> resultList = q.getResultList();
		FriendList friendList = new FriendList();
		
		for (Guest f : resultList) {
			friendList.friends.add(new GuestOutDTO(f, vdl.getNumberOfVisits(f)));
		}
		
		return friendList;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public FriendList getNotFriendsFor(Integer id) {
		User u = udl.findById(id);
		Guest g = (Guest) u;

		if (g == null) {
			return null;
		}

		List<Guest> allGuests = findAll();

		FriendList all = new FriendList();
		for (Guest gx : allGuests) {
			if (gx.getActivated().booleanValue() == true) {
				all.friends.add(new GuestOutDTO(gx, vdl.getNumberOfVisits(gx)));
			}
		}

		all.friends.remove(new GuestOutDTO(g, vdl.getNumberOfVisits(g)));
		FriendList friends = getFriendsFor(g.getId());
		all.friends.removeAll(friends.friends);

		return all;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Guest register(GuestDTO dto) {
		if (pattern.matcher(dto.getUsername()).find()) {
			Guest g = dto.convertToGuest();

			g.setToken(UserDaoBean.issueToken(g.getUsername()));
			g = persist(g);

			return g;
		} else {
			return null;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Guest activate(String token) {
		User u = udl.findUserByToken(token);

		if (u != null && u instanceof Guest) {
			Guest g = (Guest) u;
			g.setActivated(true);

			g = (Guest) udl.merge(g);

			return g;
		} else {
			return null;
		}
	}

	@Override
	public List<Guest> getFriendsOfMineFor(Integer id) {
		User u = udl.findById(id);
		Guest g = (Guest) u;

		if (g == null) {
			return null;
		}

		TypedQuery<Guest> q = em.createNamedQuery("getFriendsOfMineFor", Guest.class);
		q.setParameter("guest", g);

		return q.getResultList();
	}

}
