package dao;

import java.io.Serializable;

import entity.User;
import services.Credentials;

public interface UserDaoLocal extends GenericDaoLocal<User, Serializable> {
	public User findUserByUsernameAndPassword(String username, String password);
	public User findUserByToken(String token);
	public String authenticate(Credentials credentials) throws Exception;
	public void logout(String token) throws Exception;
	public User loginStatus(String token);
}
