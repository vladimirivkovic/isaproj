package dao;

import java.io.Serializable;

import dto.RestaurantDTO;
import dto.RestaurantList;
import entity.Restaurant;

public interface RestaurantDaoLocal extends GenericDaoLocal<Restaurant, Serializable> {
	public RestaurantList getList();
	public Restaurant addRestaurant(RestaurantDTO dto);
	public Restaurant updateRestaurant(RestaurantDTO dto, Restaurant r);
}
