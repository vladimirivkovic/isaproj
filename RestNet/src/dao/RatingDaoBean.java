package dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.TypedQuery;

import dto.RestaurantRatingList;
import dto.RestaurantWithRating;
import entity.Guest;
import entity.Rating;
import entity.Restaurant;
import entity.Visit;

@Stateless
@Local(RatingDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RatingDaoBean extends GenericDaoBean<Rating, Serializable> implements RatingDaoLocal {

	@EJB
	VisitDaoLocal vdl;

	@EJB
	GuestDaoLocal gdl;

	@EJB
	RestaurantDaoLocal rstdl;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Rating getFriendsRating(Guest g, Restaurant r) {
		TypedQuery<Rating> q = em.createNamedQuery("getFriendsRating", Rating.class);
		q.setParameter("rst", r);
		q.setParameter("gst", g);

		try {
			return q.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public void setRating(Integer rating, Guest g, Integer id) {
		Visit v = vdl.findById(id);

		if (v.getGuest().getId().equals(g.getId()) && v.getRated().booleanValue() == false
				&& v.getReservation().getDate2().before(new Date())) {
			v.setRating(rating);
			v.setRated(true);

			Restaurant rst = v.getReservation().getRestaurant();

			rst.setNumberOfRatings(rst.getNumberOfRatings() + new Integer(1));
			rst.setTotalRating(rst.getTotalRating() + rating);

			v = vdl.merge(v);

			List<Guest> friends = gdl.getFriendsOfMineFor(g.getId());
			for (Guest f : friends) {
				// find rating for f and v.rest
				Rating rat = getFriendsRating(f, rst);

				if (rat == null) {
					rat = new Rating();
					rat.setGuest(f);
					rat.setRestaurant(rst);

					rat.setNumberOfRatings(1);
					rat.setTotalRating(rating);

					persist(rat);
				} else {
					rat.setNumberOfRatings(rat.getNumberOfRatings() + 1);
					rat.setTotalRating(rst.getTotalRating() + rating);

					merge(rat);
				}
			}
			rstdl.merge(rst);
		}

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public RestaurantRatingList getListWithRatings(Guest g) {
		List<Restaurant> restaurants = rstdl.findAll();
		RestaurantRatingList rrl = new RestaurantRatingList();
		
		for (Restaurant r : restaurants) {
			Rating rat = getFriendsRating(g, r);
			rrl.restaurants.add(new RestaurantWithRating(r, rat));
		}
		
		return rrl;
	}
}
