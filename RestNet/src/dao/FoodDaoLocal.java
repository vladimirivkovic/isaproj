package dao;

import java.io.Serializable;
import java.util.List;

import dto.FoodDTO;
import entity.Food;
import entity.Restaurant;

public interface FoodDaoLocal extends GenericDaoLocal<Food, Serializable>{
	public List<Food> getMenuFor(Restaurant r);
	public Food addFood(FoodDTO dto, Restaurant r);
	public Food updateFood(FoodDTO dto, Restaurant r, Integer id);
	public Food removeFood(Restaurant r, Integer id);
}
