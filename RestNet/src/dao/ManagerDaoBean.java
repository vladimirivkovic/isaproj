package dao;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import dto.ManagerDTO;
import dto.ManagerList;
import entity.Manager;
import entity.Restaurant;

@Stateless
@Local(ManagerDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ManagerDaoBean extends GenericDaoBean<Manager, Serializable> implements ManagerDaoLocal {

	@EJB
	RestaurantDaoLocal rdl;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public ManagerList getList() {
		return new ManagerList(findAll());
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Manager addManager(ManagerDTO managerDTO) {
		Manager m = managerDTO.convertToManager();

		Restaurant r = rdl.findById(managerDTO.getRestaurant());
		if (r == null) {
			return null;
		}

		m.setRestaurant(r);
		return persist(m);
	}

}
