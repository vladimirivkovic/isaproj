package dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.TypedQuery;

import dto.FoodDTO;
import entity.Food;
import entity.Restaurant;

@Stateless
@Local(FoodDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FoodDaoBean extends GenericDaoBean<Food, Serializable> implements FoodDaoLocal {

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public List<Food> getMenuFor(Restaurant r) {
		TypedQuery<Food> q = em.createNamedQuery("getMenuFor", Food.class);
		q.setParameter("rest", r);
		
		return q.getResultList();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Food addFood(FoodDTO dto, Restaurant r) {
		Food f = dto.convertToFood();
		f.setRestaurant(r);
		return persist(f);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Food updateFood(FoodDTO dto, Restaurant r, Integer id) {
		Food f = findById(id);
		if (!f.getRestaurant().getId().equals(r.getId())) {
			return null;
		}
		
		f = dto.updateFood(f);
		return merge(f);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public Food removeFood(Restaurant r, Integer id) {
		Food f = findById(id);
		if (!f.getRestaurant().getId().equals(r.getId())) {
			return null;
		}
		
		remove(f);
		return f;
	}

}
