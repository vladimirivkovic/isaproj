package dao;

import java.io.Serializable;

import entity.Reservation;

public interface ReservationDaoLocal extends GenericDaoLocal<Reservation, Serializable> {

}
