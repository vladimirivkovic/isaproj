package dao;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.TypedQuery;

import entity.Guest;
import entity.User;
import services.Credentials;

@Stateless
@Local(UserDaoLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UserDaoBean extends GenericDaoBean<User, Serializable> implements UserDaoLocal {

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public User findUserByUsernameAndPassword(String username, String password) {
		TypedQuery<User> q = em.createNamedQuery("findUserByUsernameAndPassword", User.class);
		q.setParameter("username", username);
		q.setParameter("password", password);
		
		return q.getSingleResult();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@SuppressWarnings("finally")
	@Override
	public User findUserByToken(String token) {
		if (token == null || "".equals(token)) {
			return null;
		}
		
		TypedQuery<User> q = em.createNamedQuery("findUserByToken", User.class);
		q.setParameter("tokenValue", token);
		
		User u = null;
		
		try {
			u = q.getSingleResult();
		} catch (Exception e) {
		} finally {
			return u;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public String authenticate(Credentials credentials)  throws Exception {
		User user = findUserByUsernameAndPassword(credentials.getUsername(), credentials.getPassword());
		
		if(user instanceof Guest) {
			if (((Guest) user).getActivated().booleanValue() == false) {
				return null;
			}
		}

		if(user.getToken() == null) {
			String token = issueToken(credentials.getUsername());
			user.setToken(token);
			
			merge(user);
			
			return token;
		} else {
			return user.getToken();
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public void logout(String token) throws Exception {
		User user = findUserByToken(token);
		user.setToken(null);
		merge(user);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public static String issueToken(String username) {
    	Random random = new SecureRandom();
        String token = new BigInteger(198, random).toString(32);
        
        System.out.println("Token generated: " + token);
        
        return token;
    }

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public User loginStatus(String token) {	
		User user = findUserByToken(token);
		
		if(user instanceof Guest) {
			if(!((Guest) user).getActivated().booleanValue()) {
				return null;
			}
		}
		
		return user;
	}

}
