package services;

import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import dao.InvitationDaoLocal;
import dao.ReservationDaoLocal;
import dao.UserDaoLocal;
import dao.VisitDaoLocal;
import dto.InvitationAnswer;
import dto.InvitationDTO;
import dto.InvitationList;
import entity.Guest;
import entity.Invitation;
import entity.User;

@Path("rest/invite")
public class InvitationService {
	@EJB
	UserDaoLocal udl;

	@EJB
	InvitationDaoLocal idl;

	@EJB
	ReservationDaoLocal rsvdl;

	@EJB
	VisitDaoLocal vdl;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inviteFriends(@Context HttpServletRequest request, InvitationList invitationList) {
		if (!invitationList.validate()) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null && user instanceof Guest) {
			idl.inviteFriends(invitationList);

			return Response.ok().build();
		} else

			return Response.status(Response.Status.UNAUTHORIZED).build();
	}

	@GET
	@Path("respond/{id}")
	public Response getRespond(@Context HttpServletRequest request, @PathParam("id") Integer id) {
		System.out.println("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
		try {
			Invitation inv = idl.findById(id);
			// System.out.println(inv.getId() + " : " + inv.getResponded());
			if (inv != null) {
				Guest g = idl.getRespond(inv);
				
				request.getSession().invalidate();
				request.getSession().setAttribute(AuthService.TOKEN, g.getToken());
				System.out.println(inv.getId() + " : " + inv.getResponded());

				if (!inv.getResponded().booleanValue() && inv.getReservation().getDate1().after(new Date())) {
					return Response.status(301).header("Location", "/#/invite/" + inv.getId()).build();
				} else {
					return Response.status(301).header("Location", "/").build();
				}
			}

			return Response.status(301).header("Location", "/").build();
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@POST
	@Path("respond")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response respond(@Context HttpServletRequest request, InvitationAnswer answer) {
		if (!answer.validate()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);
		try {
			if (user != null && user instanceof Guest) {
				if (idl.respond(answer, (Guest) user)) {
					return Response.ok().build();
				}
				return Response.status(Response.Status.NOT_FOUND).build();
			} else {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@GET
	@Path("info/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInfo(@Context HttpServletRequest request, @PathParam("id") Integer id) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		try {
			if (user != null && user instanceof Guest) {
				Invitation inv = idl.findById(id);
				if (inv == null) {
					return Response.status(Response.Status.NOT_FOUND).build();
				}

				Guest g = inv.getGuest();

				if (!user.getId().equals(g.getId())) {
					return Response.status(Response.Status.UNAUTHORIZED).build();
				}

				if (inv.getResponded().booleanValue() == true) {
					return Response.status(Response.Status.NOT_FOUND).build();
				}

				return Response.ok(new InvitationDTO(inv)).build();
			}
			return Response.status(Response.Status.UNAUTHORIZED).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}
}
