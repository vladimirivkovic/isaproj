package services;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.GuestDaoLocal;
import dao.RatingDaoLocal;
import dao.RestaurantDaoLocal;
import dao.UserDaoLocal;
import dao.VisitDaoLocal;
import dto.VisitDTO;
import dto.VisitList;
import entity.Guest;
import entity.User;

@Path("rest/visits")
public class VisitService {
	@EJB
	GuestDaoLocal gdl;

	@EJB
	UserDaoLocal udl;

	@EJB
	VisitDaoLocal vdl;

	@EJB
	RestaurantDaoLocal rstdl;

	@EJB
	RatingDaoLocal radl;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getVisits(@Context HttpServletRequest request) {
		try {
			String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
			User user = udl.loginStatus(token);

			if (user != null && user instanceof Guest) {
				List<VisitDTO> visits = vdl.findVisitsFor((Guest) user);
				return Response.ok(new VisitList(visits)).build();
			} else {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@POST
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response setRating(@Context HttpServletRequest request, @PathParam("id") Integer id, String ratingString) {
		try {
			String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
			User user = udl.loginStatus(token);

			if (user != null && user instanceof Guest) {
				Integer rating = new Integer(ratingString);
				Guest g = (Guest) user;

				radl.setRating(rating, g, id);
				return Response.ok().build();
			} else {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}
}
