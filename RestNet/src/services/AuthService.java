package services;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.UserDaoLocal;
import dto.StatusResponse;
import entity.User;

@Path("/rest/auth")
public class AuthService {

	public static final String TOKEN = "token";

	@EJB
	UserDaoLocal udl;

	@GET
	@Path("status")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response status(@Context HttpServletRequest request) {
		String token = (String) request.getSession().getAttribute(TOKEN);
		User user = udl.loginStatus(token);

		if (user != null)
			return Response.ok(new StatusResponse(user)).build();
		else
			return Response.status(Response.Status.UNAUTHORIZED).build();
	}

	@POST
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(@Context HttpServletRequest request, Credentials credentials) {
		try {
			String token = udl.authenticate(credentials);

			request.getSession().setAttribute(TOKEN, token);
			return Response.ok().build();
		} catch (Exception e) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@POST
	@Path("logout")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response logout(@Context HttpServletRequest request) {
		try {
			String token = (String) request.getSession().getAttribute(TOKEN);

			udl.logout(token);
			request.getSession().invalidate();

			return Response.ok().build();
		} catch (Exception e) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	// @GET
	// @Produces(MediaType.TEXT_HTML)
	// public String registerAdmin() {
	// Admin admir = new Admin();
	// admir.setUsername("admin");
	// admir.setPassword("admin");
	// System.out.println(admir.getRole());
	// admir.setToken(null);
	//
	// admir = (Admin) udl.persist(admir);
	//
	// System.out.println(admir.getRole());
	//
	// return "admir registrated";
	// }

}
