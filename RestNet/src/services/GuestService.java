package services;

import java.net.URISyntaxException;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.FriendshipDaoLocal;
import dao.GuestDaoLocal;
import dao.UserDaoLocal;
import dao.VisitDaoLocal;
import dto.FriendList;
import dto.GuestDTO;
import dto.GuestOutDTO;
import entity.Guest;
import entity.User;
import mdb.SendLocal;

@Path("rest/guests")
public class GuestService {

	@EJB
	GuestDaoLocal gdl;

	@EJB
	SendLocal sl;

	@EJB
	UserDaoLocal udl;

	@EJB
	FriendshipDaoLocal fdl;

	@EJB
	VisitDaoLocal vdl;

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGuests(@Context HttpServletRequest request) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user instanceof User)
			return Response.ok(new FriendList(gdl.findAll())).build();
		else
			return Response.status(Response.Status.UNAUTHORIZED).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response register(GuestDTO dto) {
		if(!dto.validate())	{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		Guest g = gdl.register(dto);

		if (g != null) {
			sl.sendRegistationMail(g.getToken(), g.getUsername());
			return Response.ok().build();
		} else {
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}

	@GET
	@Path("activate/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response activate(@Context HttpServletRequest request, @PathParam("id") String id)
			throws URISyntaxException {
		try {
			Guest g = gdl.activate(id);

			if (g != null) {
				request.getSession().setAttribute(AuthService.TOKEN, g.getToken());
				return Response.status(301).header("Location", "/").build();
			} else {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}

	@GET
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGuest(@Context HttpServletRequest request, @PathParam("id") Integer id) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user == null)
			return Response.status(Response.Status.UNAUTHORIZED).build();

		try {
			User u = udl.findById(id);
			return Response.ok(new GuestOutDTO((Guest) u, vdl.getNumberOfVisits((Guest) u))).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateGuest(@Context HttpServletRequest request, @PathParam("id") Integer id, GuestDTO dto) {
		if(!dto.validate())	{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user == null || !(user instanceof Guest))
			return Response.status(Response.Status.UNAUTHORIZED).build();

		try {
			User u = udl.findById(id);
			Guest g = dto.updateGuest((Guest) u);
			if (g != null) {
				gdl.merge(g);
			}

			return Response.ok(new GuestOutDTO(g, vdl.getNumberOfVisits(g))).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}

	@GET
	@Path("{id}/friends")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFriends(@Context HttpServletRequest request, @PathParam("id") Integer id) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user == null || !(user instanceof Guest))
			return Response.status(Response.Status.UNAUTHORIZED).build();

		try {
			return Response.ok(gdl.getFriendsFor(id)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@GET
	@Path("{id}/notfriends")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNotFriends(@Context HttpServletRequest request, @PathParam("id") Integer id) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user == null || !(user instanceof Guest))
			return Response.status(Response.Status.UNAUTHORIZED).build();

		try {
			FriendList friends = gdl.getNotFriendsFor(id);
			return Response.ok(friends).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}

	@POST
	@Path("{id}/friends")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addFriend(@Context HttpServletRequest request, @PathParam("id") Integer id, Integer fid) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user == null || !(user instanceof Guest))
			return Response.status(Response.Status.UNAUTHORIZED).build();

		User u = udl.findById(id);
		if (!u.equals(user)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			fdl.makeFriends(id, fid);

			return Response.ok(gdl.getFriendsFor(id)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}

	@DELETE
	@Path("{id}/friends/{fid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeFriend(@Context HttpServletRequest request, @PathParam("id") Integer id, Integer fid) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user == null || !(user instanceof Guest))
			return Response.status(Response.Status.UNAUTHORIZED).build();

		User u = udl.findById(id);
		if (!u.equals(user)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			fdl.removeFriends(id, fid);

			return Response.ok(gdl.getFriendsFor(id)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}
}
