package services;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.ManagerDaoLocal;
import dao.RestaurantDaoLocal;
import dao.UserDaoLocal;
import dto.ManagerDTO;
import entity.Admin;
import entity.Guest;
import entity.Manager;
import entity.User;

@Path("/rest/managers")
public class ManagerService {

	@EJB
	UserDaoLocal udl;
	
	@EJB
	RestaurantDaoLocal rdl;
	
	@EJB
	ManagerDaoLocal mdl;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getManagers(@Context HttpServletRequest request) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);
		
		if (user != null && user instanceof Admin) {
			return Response.ok(mdl.getList()).build();
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addManager(@Context HttpServletRequest request, ManagerDTO managerDTO) {
		if(!managerDTO.validate()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);
		
		if (user != null && user instanceof Admin) {
			Manager m = mdl.addManager(managerDTO);
			
			return Response.ok(m).build();
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getManager(@Context HttpServletRequest request, @PathParam(value = "id") Integer id) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);
		
		if (user != null && !(user instanceof Guest)) {
			return Response.ok(mdl.findById(id)).build();
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

}
