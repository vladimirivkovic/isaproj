package services;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.FoodDaoLocal;
import dao.RatingDaoLocal;
import dao.RestaurantDaoLocal;
import dao.TableDaoLocal;
import dao.UserDaoLocal;
import dto.FoodDTO;
import dto.Menu;
import dto.RestaurantDTO;
import dto.TableDTO;
import dto.TableList;
import entity.Admin;
import entity.Food;
import entity.Guest;
import entity.Manager;
import entity.Restaurant;
import entity.TableX;
import entity.User;

@Path("/rest/restaurants")
public class RestaurantService {

	@EJB
	UserDaoLocal udl;

	@EJB
	RestaurantDaoLocal rdl;

	@EJB
	FoodDaoLocal fdl;

	@EJB
	TableDaoLocal tdl;
	
	@EJB
	RatingDaoLocal radl;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getRestaurants(@Context HttpServletRequest request) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null) {
			if(user instanceof Guest) {
				return Response.ok(radl.getListWithRatings((Guest) user)).build();
			}
			return Response.ok(rdl.getList()).build();
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addRestaurant(@Context HttpServletRequest request, RestaurantDTO restaurantDTO) {
		if(!restaurantDTO.validate()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null && user instanceof Admin) {
			return Response.ok(rdl.addRestaurant(restaurantDTO)).build();
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@PUT
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateRestaurant(@Context HttpServletRequest request, RestaurantDTO restaurantDTO,
			@PathParam(value = "id") Integer id) {
		if(!restaurantDTO.validate()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null && user instanceof Manager) {
			try {
				Restaurant r = rdl.findById(((Manager) user).getRestaurant().getId());

				if (r == null || !id.equals(r.getId())) {
					return Response.status(Response.Status.NOT_FOUND).build();
				}

				return Response.ok(rdl.updateRestaurant(restaurantDTO, r)).build();
			} catch (Exception e) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getRestaurant(@Context HttpServletRequest request, @PathParam(value = "id") Integer id) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null) {
			try {
				return Response.ok(rdl.findById(id)).build();
			} catch (Exception e) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

	}

	@GET
	@Path("{id}/menu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getMenu(@Context HttpServletRequest request, @PathParam(value = "id") Integer id) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null) {
			try {
				Restaurant r = rdl.findById(id);

				return Response.ok(new Menu(fdl.getMenuFor(r))).build();
			} catch (Exception e) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

	}

	@POST
	@Path("{id}/menu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addFood(@Context HttpServletRequest request, @PathParam(value = "id") Integer id, FoodDTO foodDTO) {
		if(!foodDTO.validate()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null && user instanceof Manager) {
			try {
				Restaurant r = rdl.findById(((Manager) user).getRestaurant().getId());

				if (r == null || !id.equals(r.getId())) {
					return Response.status(Response.Status.NOT_FOUND).build();
				}

				Food f = fdl.addFood(foodDTO, r);
				return Response.ok(f).build();
			} catch (Exception e) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@PUT
	@Path("{id}/menu/{fid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateFood(@Context HttpServletRequest request, @PathParam(value = "id") Integer id,
			@PathParam(value = "fid") Integer fid, FoodDTO foodDTO) {
		if(!foodDTO.validate()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null && user instanceof Manager) {
			try {
				Restaurant r = rdl.findById(((Manager) user).getRestaurant().getId());
				if (!id.equals(r.getId())) {
					return Response.status(Response.Status.UNAUTHORIZED).build();
				}

				Food f = fdl.updateFood(foodDTO, r, fid);
				if (f == null) {
					return Response.status(Response.Status.NOT_FOUND).build();
				}
				return Response.ok(f).build();
			} catch (Exception e) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@DELETE
	@Path("{id}/menu/{fid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeFood(@Context HttpServletRequest request, @PathParam(value = "id") Integer id,
			@PathParam(value = "fid") Integer fid) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null && user instanceof Manager) {
			try {
				Restaurant r = rdl.findById(((Manager) user).getRestaurant().getId());
				if (!id.equals(r.getId())) {
					return Response.status(Response.Status.UNAUTHORIZED).build();
				}

				Food f = fdl.removeFood(r, fid);
				return Response.ok(f).build();
			} catch (Exception e) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@POST
	@Path("{id}/tables")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addTables(@Context HttpServletRequest request, @PathParam(value = "id") Integer id,
			TableList tableList) {
		if(!tableList.validate()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null && user instanceof Manager) {
			Restaurant r = rdl.findById(id);

			if (r == null || !id.equals(r.getId())) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}

			tdl.configTables(r, tableList);
			return Response.ok(tableList).build();
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@GET
	@Path("{id}/tables")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getTables(@Context HttpServletRequest request, @PathParam(value = "id") Integer id) {
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);

		if (user != null) {
			Restaurant r = rdl.findById(id);
			TableList list = new TableList();

			if (r != null && r.getConfigured() != null && r.getConfigured().booleanValue() == true) {

				List<TableX> tables = tdl.getTableIn(r);

				for (TableX tx : tables) {
					list.tables.add(new TableDTO(tx));
				}

			}
			return Response.ok(list).build();
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}
}
