package services;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import mdb.SendLocal;

@Path("/rest/mail")
public class MailService {

	@EJB
	SendLocal sl;
	
	@GET
	public void test() {
		sl.test();
	}
}
