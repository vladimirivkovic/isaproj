package services;

import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.GuestDaoLocal;
import dao.ReservationDaoLocal;
import dao.ReservedTableDaoLocal;
import dao.RestaurantDaoLocal;
import dao.TableDaoLocal;
import dao.UserDaoLocal;
import dao.VisitDaoLocal;
import dto.ReservationDTO;
import entity.Guest;
import entity.Reservation;
import entity.Restaurant;
import entity.User;

@Path("/rest/reserve")
public class ReservationService {

	@EJB
	ReservationDaoLocal rsvdl;

	@EJB
	RestaurantDaoLocal rstdl;

	@EJB
	GuestDaoLocal gdl;

	@EJB
	TableDaoLocal tdl;

	@EJB
	ReservedTableDaoLocal rtdl;
	
	@EJB
	VisitDaoLocal vdl;
	
	@EJB
	UserDaoLocal udl;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response reserve(@Context HttpServletRequest request, ReservationDTO reservationDTO) {
		if(!reservationDTO.validate()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);
		
		if(user != null && user instanceof Guest) {
			Guest g = (Guest) user;
			Restaurant rst = rstdl.findById(reservationDTO.getRestaurantId());

			System.out.println(g.getName() + " oce da rezervise");
			System.out.println(rst.getName() + " mjesto u ovom restoranu");
			System.out.println(new Date(reservationDTO.getDate1()) + " do " + new Date(reservationDTO.getDate1()));
			System.out.println(reservationDTO.getTableIds().size() + " stolova");

			Reservation rsv = rtdl.reserve(g, reservationDTO);
			
			if(rsv == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			
			return Response.ok("{\"id\":" + rsv.getId() + "}").build();
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@POST
	@Path("check")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkReserved(@Context HttpServletRequest request, ReservationDTO reservationDTO) {
		if(!reservationDTO.validateCheck()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		String token = (String) request.getSession().getAttribute(AuthService.TOKEN);
		User user = udl.loginStatus(token);
		
		if(user != null && user instanceof Guest) {
			Guest g = gdl.findById(reservationDTO.getGuestId());
			Restaurant rst = rstdl.findById(reservationDTO.getRestaurantId());

			System.out.println(g.getName() + " oce da rezervise");
			System.out.println(rst.getName() + " mesto u ovom restoranu");
			System.out.println(new Date(reservationDTO.getDate1()) + " do " + new Date(reservationDTO.getDate2()));

			reservationDTO = rtdl.checkReserve(g, reservationDTO);
			if(reservationDTO == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}

			return Response.ok(reservationDTO).build();
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

}
