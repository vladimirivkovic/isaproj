package mdb;


public interface SendLocal {
	public void test();
	public void sendRegistationMail(String t, String email);
	public void sendInvitation(String key, String email);
}
