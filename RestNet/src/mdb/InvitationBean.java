package mdb;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "InvitationBean") })
public class InvitationBean implements MessageListener {

	@Resource(name = "Mail")
	Session session;

	public InvitationBean() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onMessage(Message message) {

		try {
			if (message instanceof ObjectMessage) {
				ObjectMessage obj = (ObjectMessage) message;

				String key = (String) obj.getObjectProperty("key");
				String e = (String) obj.getObjectProperty("email");

				// Validate the credit card using web services...

				sendMessage(key, e);
			} else {
				System.out.println("MESSAGE BEAN: Message of wrong type: " + message.getClass().getName());
			}
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (Throwable te) {
			te.printStackTrace();
		}

	}

	private void sendMessage(String key, String e) throws AddressException, MessagingException {

		// Constructs the message
		javax.mail.Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("vlado.ivkovic993@gmail.com"));
		msg.setRecipients(RecipientType.TO, InternetAddress.parse(e));
		msg.setSubject("Invitation");
		msg.setText("Follow this link : <a href=\"http://localhost:8080/RestNet/rest/invite/respond/" + key
				+ "\">here</a>");
		msg.setSentDate(new Date());
		msg.setHeader("Content-Type", "text/html");

		// Sends the message
		Transport.send(msg);

		System.out.println("MESSAGE BEAN: Mail was sent successfully.");
	}

}
