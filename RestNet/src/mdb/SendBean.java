package mdb;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

@Stateless
@Local(SendLocal.class)
public class SendBean implements SendLocal {

	@Resource(name = "JmsConnectionFactory")
	private ConnectionFactory qcf;

	@Resource(name = "RegistrationBean")
	private Queue registrationQueue;
	
	@Resource(name = "InvitationBean")
	private Queue invitationQueue;

	public void test() {

		Connection connection = null;
		Session session = null;
		MessageProducer producer = null;

		try {
			// Creates a connection
			connection = qcf.createConnection();

			// Creates a session
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Creates a message producer from the Session to the Topic or Queue
			producer = session.createProducer(registrationQueue);

			// Creates an object message
			ObjectMessage object = session.createObjectMessage();
			object.setObject("evo");

			// Tells the producer to send the object message
			producer.send(object);

			// Closes the producer
			producer.close();

			// Closes the session
			session.close();

			// Closes the connection
			connection.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendRegistationMail(String t, String e) {
		Connection connection = null;
		Session session = null;
		MessageProducer producer = null;

		try {
			// Creates a connection
			connection = qcf.createConnection();

			// Creates a session
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Creates a message producer from the Session to the Topic or Queue
			producer = session.createProducer(registrationQueue);

			// Creates an object message
			ObjectMessage object = session.createObjectMessage();
			object.setObjectProperty("token", t);
			object.setObjectProperty("email", e);

			// Tells the producer to send the object message
			producer.send(object);

			// Closes the producer
			producer.close();

			// Closes the session
			session.close();

			// Closes the connection
			connection.close();
		} catch (JMSException ex) {
			ex.printStackTrace();
		}
		
	}

	@Override
	public void sendInvitation(String key, String email) {
		Connection connection = null;
		Session session = null;
		MessageProducer producer = null;

		try {
			// Creates a connection
			connection = qcf.createConnection();

			// Creates a session
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Creates a message producer from the Session to the Topic or Queue
			producer = session.createProducer(invitationQueue);

			// Creates an object message
			ObjectMessage object = session.createObjectMessage();
			object.setObjectProperty("key", key);
			object.setObjectProperty("email", email);

			// Tells the producer to send the object message
			producer.send(object);

			// Closes the producer
			producer.close();

			// Closes the session
			session.close();

			// Closes the connection
			connection.close();
		} catch (JMSException ex) {
			ex.printStackTrace();
		}
		
	}
}
