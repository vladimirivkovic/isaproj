package dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RestaurantRatingList {
	@XmlElement
	public List<RestaurantWithRating> restaurants = new ArrayList<>();
	
	public RestaurantRatingList(List<RestaurantWithRating> restaurants) {
		this.restaurants = restaurants;
	}
	
	public RestaurantRatingList() {
		// TODO Auto-generated constructor stub
	}
}
