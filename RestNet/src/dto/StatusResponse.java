package dto;

import javax.xml.bind.annotation.XmlRootElement;

import entity.Manager;
import entity.User;

@XmlRootElement
public class StatusResponse {
	private Integer id;
	private String role;
	private Integer restId;

	public StatusResponse() {
		// TODO Auto-generated constructor stub
	}

	public StatusResponse(User user) {
		this.setId(user.getId());
		this.setRole(user.getRole());
		if (user instanceof Manager) {
			this.setRestId(((Manager) user).getRestaurant().getId());
		} else {
			this.setRestId(null);
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRestId() {
		return restId;
	}

	public void setRestId(Integer restId) {
		this.restId = restId;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}
}
