package dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class ReservationDTO {
	private int restaurantId;
	private int guestId;
	@XmlTransient
	private List<Integer> tableIds = new ArrayList<>();
	private long date1;
	private long date2;

	public ReservationDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getGuestId() {
		return guestId;
	}

	public void setGuestId(Integer guestId) {
		this.guestId = guestId;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public List<Integer> getTableIds() {
		return tableIds;
	}

	public void setTableIds(List<Integer> tableIds) {
		this.tableIds = tableIds;
	}

	public Long getDate1() {
		return date1;
	}

	public void setDate1(Long date1) {
		this.date1 = date1;
	}

	public Long getDate2() {
		return date2;
	}

	public void setDate2(Long date2) {
		this.date2 = date2;
	}

	public void removeDuplicates() {
		List<Integer> newList = new ArrayList<>();

		for (Integer i : tableIds) {
			if (!newList.contains(i)) {
				newList.add(i);
			}
		}

		tableIds = newList;

	}

	@Override
	public String toString() {
		return guestId + "oce da rezervise" + restaurantId;
	}

	public boolean validate() {
		if(date1 >= date2) {
			return false;
		}
		if (tableIds == null || tableIds.size() == 0)
			return false;
		return true;
	}

	public boolean validateCheck() {
		if(date1 >= date2) {
			return false;
		}
		return true;
	}
}
