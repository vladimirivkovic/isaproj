package dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="tableList")
@XmlAccessorType(XmlAccessType.FIELD)
public class TableList {
	@XmlElement(name="tables")
	public List<TableDTO> tables = new ArrayList<>();
	
	public TableList() {
		// TODO Auto-generated constructor stub
	}
	
	public TableList(List<TableDTO> list) {
		tables = list;
	}

	public boolean validate() {
		if(tables == null || tables.size() == 0) {
			return false;
		}
		
		for(TableDTO t : tables) {
			if(!t.validate()) {
				return false;
			}
		}
		
		return true;
	}
}
