package dto;

import javax.xml.bind.annotation.XmlRootElement;

import entity.Guest;

@XmlRootElement
public class GuestOutDTO {
	private Integer id;
	private String name;
	private String surname;
	private String username;
	private String address;
	private Integer numberOfVisits = 0;

	public GuestOutDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getNumberOfVisits() {
		return numberOfVisits;
	}

	public void setNumberOfVisits(Integer numberOfVisits) {
		this.numberOfVisits = numberOfVisits;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public GuestOutDTO(Guest g, Integer nov) {
		id = g.getId();
		name = g.getName();
		surname = g.getSurname();
		address = g.getAddress();
		username = g.getUsername();
		numberOfVisits = nov;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GuestOutDTO) {
			return id.equals(((GuestOutDTO) obj).getId());
		} else {
			return false;
		}
	}
}
