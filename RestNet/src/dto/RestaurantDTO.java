package dto;

import javax.xml.bind.annotation.XmlRootElement;

import entity.Restaurant;

@XmlRootElement
public class RestaurantDTO {
	private String name;
	private String type;
	private Double longitude;
	private Double latitude;

	public RestaurantDTO() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Restaurant convertToRestaurant() {
		Restaurant r = new Restaurant();

		r.setName(name);
		r.setType(type);
		r.setLongitude(longitude);
		r.setLatitude(latitude);
		r.setNumberOfRatings(0);
		r.setTotalRating(0);
		r.setConfigured(false);

		return r;
	}

	public Restaurant updateRestaurant(Restaurant r) {
		r.setName(name);
		r.setType(type);
		r.setLongitude(longitude);
		r.setLatitude(latitude);

		return r;
	}

	public boolean validate() {
		if (name == null || "".equals(name)) {
			return false;
		}

		if (type == null || "".equals(type)) {
			return false;
		}

		if (longitude == null || longitude < -180 || longitude > 180) {
			return false;
		}

		if (latitude == null || latitude < -90 || latitude > 90) {
			return false;
		}

		return true;
	}
}
