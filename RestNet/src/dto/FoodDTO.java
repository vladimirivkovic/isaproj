package dto;

import javax.xml.bind.annotation.XmlRootElement;

import entity.Food;

@XmlRootElement
public class FoodDTO {
	private String name;
	private String description;
	private Double price;
	
	public FoodDTO() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	public Food convertToFood() {
		Food f = new Food();
		
		f.setName(name);
		f.setDescription(description);
		f.setPrice(price);
		
		return f;
	}
	
	public Food updateFood(Food f) {
		f.setName(name);
		f.setDescription(description);
		f.setPrice(price);
		
		return f;
	}

	public boolean validate() {
		if(name == null || "".equals(name)) {
			return false;
		}
		
		if(description == null || "".equals(description)) {
			return false;
		}
		
		if(price == null || price < 0) {
			return false;
		}
		
		return true;
	}
}
