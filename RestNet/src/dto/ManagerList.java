package dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import entity.Manager;

@XmlRootElement
public class ManagerList {
	@XmlElement
	public List<Manager> managers = new ArrayList<>();
	
	public ManagerList() {
		// TODO Auto-generated constructor stub
	}
	
	public ManagerList(List<Manager> list) {
		this.managers = list;
	}

}
