package dto;

import entity.Rating;
import entity.Restaurant;

public class RestaurantWithRating {
	private Integer id;
	private String name;
	private String type;
	private Double longitude;
	private Double latitude;
	private Double totalRating;
	private Double friendsRating;
	private Boolean configured;

	public RestaurantWithRating() {
		// TODO Auto-generated constructor stub
	}

	public RestaurantWithRating(Restaurant res, Rating rat) {
		id = res.getId();
		name = res.getName();
		type = res.getType();
		longitude = res.getLongitude();
		latitude = res.getLatitude();
		totalRating = new Double(res.getTotalRating()) / res.getNumberOfRatings();
		if (rat != null)
			friendsRating = new Double(rat.getTotalRating()) / rat.getNumberOfRatings();
		else
			friendsRating = null;
		configured = res.getConfigured();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(Double totalRating) {
		this.totalRating = totalRating;
	}

	public Double getFriendsRating() {
		return friendsRating;
	}

	public void setFriendsRating(Double friendsRating) {
		this.friendsRating = friendsRating;
	}

	public Boolean getConfigured() {
		return configured;
	}

	public void setConfigured(Boolean configured) {
		this.configured = configured;
	}

}
