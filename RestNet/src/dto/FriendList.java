package dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import entity.Guest;

@XmlRootElement
public class FriendList {
	@XmlElement
	public List<GuestOutDTO> friends = new ArrayList<>();
	
	public FriendList() {
		// TODO Auto-generated constructor stub
	}
	
	public FriendList(List<Guest> list) {
		for(Guest g : list) {
			friends.add(new GuestOutDTO(g, null));
		}
	}
	
	public FriendList remove(FriendList fl) {
		this.friends.removeAll(fl.friends);
		
		return this;
	}

}
