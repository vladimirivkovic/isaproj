package dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import entity.Food;

@XmlRootElement
public class Menu {
	@XmlElement
	public List<Food> menu = new ArrayList<>();
	
	public Menu() {
		// TODO Auto-generated constructor stub
	}
	
	public Menu(List<Food> list) {
		menu = list;
	}

}
