package dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InvitationList {
	private Integer reservationId;
	private List<Integer> guestIds;

	public InvitationList() {
		// TODO Auto-generated constructor stub
	}

	public List<Integer> getGuestIds() {
		return guestIds;
	}

	public void setGuestIds(List<Integer> guestIds) {
		this.guestIds = guestIds;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public boolean validate() {
		if (reservationId == null) {
			return false;
		}

		if (guestIds == null || guestIds.size() == 0) {
			return false;
		}

		for (Integer i : guestIds) {
			if (i == null) {
				return false;
			}
		}
		return true;
	}
}
