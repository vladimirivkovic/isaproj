package dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import entity.Restaurant;

@XmlRootElement
public class RestaurantList {
	@XmlElement
	public List<Restaurant> restaurants = new ArrayList<>();
	
	public RestaurantList(List<Restaurant> restaurants) {
		this.restaurants = restaurants;
	}
	
	public RestaurantList() {
		// TODO Auto-generated constructor stub
	}
}
