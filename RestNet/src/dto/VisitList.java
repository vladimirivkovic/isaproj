package dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class VisitList {
	public List<VisitDTO> visits = new ArrayList<VisitDTO>();
	
	public VisitList() {
		// TODO Auto-generated constructor stub
	}
	
	public VisitList(List<VisitDTO> list) {
		visits = list;
	}
}
