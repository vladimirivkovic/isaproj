package dto;

import javax.xml.bind.annotation.XmlRootElement;

import entity.Manager;

@XmlRootElement
public class ManagerDTO {
	private String username;
	private String password;
	private Integer restaurant;

	public ManagerDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Integer restaurant) {
		this.restaurant = restaurant;
	}

	public Manager convertToManager() {
		Manager m = new Manager();

		m.setUsername(username);
		m.setPassword(password);

		return m;
	}

	public boolean validate() {
		if (username == null || "".equals(username)) {
			return false;
		}

		if (password == null || "".equals(password)) {
			return false;
		}

		if (restaurant == null) {
			return false;
		}

		return true;
	}
}
