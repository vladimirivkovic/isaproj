package dto;

import javax.xml.bind.annotation.XmlRootElement;

import entity.Invitation;
import entity.Reservation;

@XmlRootElement
public class InvitationDTO {

	private Reservation reservation;
	private GuestOutDTO guest;
	private GuestOutDTO inviteFrom;

	public InvitationDTO(Invitation inv) {
		reservation = inv.getReservation();
		guest = new GuestOutDTO(inv.getGuest(), null);
		inviteFrom = new GuestOutDTO(inv.getReservation().getGuest(), null);
	}

	public InvitationDTO() {
		// TODO Auto-generated constructor stub
	}

	public GuestOutDTO getGuest() {
		return guest;
	}

	public void setGuest(GuestOutDTO guest) {
		this.guest = guest;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	
	public GuestOutDTO getInviteFrom() {
		return inviteFrom;
	}
	
	public void setInviteFrom(GuestOutDTO inviteFrom) {
		this.inviteFrom = inviteFrom;
	}
	

}
