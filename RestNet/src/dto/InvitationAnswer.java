package dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InvitationAnswer {
	private Integer invitationId;
	private Boolean ans;

	public InvitationAnswer() {
		// TODO Auto-generated constructor stub
	}

	public Boolean getAns() {
		return ans;
	}

	public void setAns(Boolean ans) {
		this.ans = ans;
	}

	public Integer getInvitationId() {
		return invitationId;
	}

	public void setInvitationId(Integer invitationId) {
		this.invitationId = invitationId;
	}

	public boolean validate() {
		if (invitationId == null) {
			return false;
		}

		if (ans == null) {
			return false;
		}

		return true;
	}

}
