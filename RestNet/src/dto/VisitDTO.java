package dto;

import java.util.ArrayList;
import java.util.List;

import entity.Guest;
import entity.Reservation;
import entity.Visit;

public class VisitDTO {
	private Integer id;
	private Integer rating;
	private Boolean rated;
	private Reservation reservation;
	private Guest guest;
	private List<String> friends = new ArrayList<>();
	
	public VisitDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public VisitDTO(Visit v) {
		id = v.getId();
		rated = v.getRated();
		rating = v.getRating();
		reservation = v.getReservation();
		guest = v.getGuest();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Boolean getRated() {
		return rated;
	}

	public void setRated(Boolean rated) {
		this.rated = rated;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}

	public List<String> getFriends() {
		return friends;
	}

	public void setFriends(List<String> friends) {
		this.friends = friends;
	}
}
