package dto;

import javax.xml.bind.annotation.XmlRootElement;

import dao.GuestDaoBean;
import entity.Guest;

@XmlRootElement
public class GuestDTO {
	private String username;
	private String password;
	private String name;
	private String surname;
	private String newPassword;
	private String address;

	public GuestDTO() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public Guest convertToGuest() {
		Guest g = new Guest();

		g.setAddress(address);
		g.setName(name);
		g.setSurname(surname);
		g.setUsername(username);
		g.setPassword(password);
		g.setActivated(false);

		return g;
	}

	public Guest updateGuest(Guest g) {
		if (password.equals(g.getPassword()) && GuestDaoBean.pattern.matcher(username).find()) {
			g.setName(name);
			g.setSurname(surname);
			g.setAddress(address);
			g.setUsername(username);
			g.setPassword(newPassword);
			return g;
		} else {
			return null;
		}
	}

	public boolean validate() {
		if(username == null || "".equals(username)) {
			return false;
		}
		if(name == null || "".equals(name)) {
			return false;
		}
		if(surname == null || "".equals(surname)) {
			return false;
		}
		if(password == null || "".equals(password)) {
			return false;
		}
		if(newPassword == null || "".equals(newPassword)) {
			return false;
		}
		if(address == null || "".equals(address)) {
			return false;
		}
		return true;
	}
}
