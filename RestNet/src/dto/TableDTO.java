package dto;

import javax.xml.bind.annotation.XmlRootElement;

import entity.TableX;

@XmlRootElement
public class TableDTO {
	private static Short xMax = 6;
	private static Short xMin = 0;
	private static Short yMax = 6;
	private static Short yMin = 0;
	
	private Integer id;
	private String name;
	private Short x;
	private Short y;
	
	public TableDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Short getX() {
		return x;
	}

	public void setX(Short x) {
		this.x = x;
	}

	public Short getY() {
		return y;
	}

	public void setY(Short y) {
		this.y = y;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public TableX convertToTable() {
		TableX t = new TableX();
		
		t.setName(name);
		t.setX(x);
		t.setY(y);
		
		return t;
	}
	
	public TableDTO(TableX tx) {
		name = tx.getName();
		x = tx.getX();
		y = tx.getY();
		id = tx.getId();
	}

	public boolean validate() {
		if(name == null || "".equals(name)) {
			return false;
		}
		
		if(x == null || !(xMin <= x && x <= xMax)) {
			return false;
		}
		
		if(y == null || !(yMin <= y && y <= yMax)) {
			return false;
		}
		
		return true;
	}
}
