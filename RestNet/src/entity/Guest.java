package entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@DiscriminatorValue("G")
@NamedQueries({
@NamedQuery(name = "getFriendsFor", query = "SELECT g FROM Guest g, Friendship f WHERE f.firstFriend = :guest AND f.secondFriend = g AND f.secondFriend.activated = true"),
@NamedQuery(name = "getFriendsOfMineFor", query = "SELECT g FROM Guest g, Friendship f WHERE f.secondFriend = :guest AND f.firstFriend = g AND f.firstFriend.activated = true"),
@NamedQuery(name = "getNotFriendsFor", query = "SELECT g FROM Guest g, Friendship f WHERE NOT(f.firstFriend = :guest AND f.secondFriend = g)")
})
public class Guest extends User {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9201137906291072226L;

	@Column(name = "GE_NAME", unique = false, nullable = false)
	private String name;
	
	@Column(name = "GE_SURNAME", unique = false, nullable = false)
	private String surname;
	
	@Column(name = "GE_ADDRESS", unique = false, nullable = false)
	private String address;
	
	@Column(name = "GE_ACTIVATED", unique = false, nullable = false)
	private Boolean activated;
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy="firstFriend")
	private Set<Friendship> myFriends = new HashSet<Friendship>();
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy="secondFriend")
	private Set<Friendship> friendOfMine = new HashSet<Friendship>();
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy="guest")
	private Set<Reservation> reservations = new HashSet<Reservation>();
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy="guest")
	private Set<Invitation> invitations = new HashSet<Invitation>();
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy="guest")
	private Set<Rating> ratings = new HashSet<Rating>();
	
	public Guest() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public Set<Friendship> getMyFriends() {
		return myFriends;
	}

	public void setMyFriends(Set<Friendship> myFriends) {
		this.myFriends = myFriends;
	}

	public Set<Friendship> getFriendOfMine() {
		return friendOfMine;
	}

	public void setFriendOfMine(Set<Friendship> friendOfMine) {
		this.friendOfMine = friendOfMine;
	}

	public Set<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(Set<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Set<Invitation> getInvitations() {
		return invitations;
	}

	public void setInvitations(Set<Invitation> invitations) {
		this.invitations = invitations;
	}

	public Set<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}
	
	
}
