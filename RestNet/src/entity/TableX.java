package entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.openjpa.persistence.jdbc.VersionColumn;

@XmlRootElement
@Entity
@Table(name="TABLEX")
@VersionColumn
@NamedQuery(name = "getTablesIn", query = "SELECT t FROM TableX t WHERE t.restaurant = :rest")
public class TableX implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8584567025277917247L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TA_ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "TA_LABEL", unique = false, nullable = false)
	private String name;
	
	@Column(name = "TA_X", unique = false, nullable = false)
	private Short x;
	
	@Column(name = "TA_Y", unique = false, nullable = false)
	private Short y;
	
	@ManyToOne
	@JoinColumn(name = "RE_ID", referencedColumnName = "RE_ID", nullable = false)
	private Restaurant restaurant;
	
	public TableX() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Short getX() {
		return x;
	}

	public void setX(Short x) {
		this.x = x;
	}

	public Short getY() {
		return y;
	}

	public void setY(Short y) {
		this.y = y;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
}
