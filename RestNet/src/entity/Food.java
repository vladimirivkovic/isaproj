package entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.openjpa.persistence.jdbc.VersionColumn;

@XmlRootElement
@Entity
@Table(name="FOOD")
@VersionColumn
@NamedQuery(name = "getMenuFor", query = "SELECT f FROM Food f WHERE f.restaurant = :rest")
public class Food implements Serializable {
	private static final long serialVersionUID = -6855488192706053357L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "FO_ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "FO_NAME", unique = false, nullable = false)
	private String name;
	
	@Column(name = "FO_DESCRIPTION", unique = false, nullable = false)
	private String description;
	
	@Column(name = "FO_PRICE", unique = false, nullable = false)
	private Double price;
	
	@ManyToOne
	@JoinColumn(name = "RE_ID", referencedColumnName = "RE_ID", nullable = false)
	private Restaurant restaurant;

	public Food() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
}
