package entity;


import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.openjpa.persistence.jdbc.VersionColumn;

/**
 * Entity implementation class for Entity: Rating
 *
 */

@XmlRootElement
@Entity
@Table(name="RATING")
@VersionColumn
@NamedQuery(name = "getFriendsRating", query = "SELECT r FROM Rating r WHERE r.restaurant = :rst AND r.guest = :gst")
public class Rating implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5387230228652042889L;

	public Rating() {
		super();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "RA_ID", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "RA_RATING", unique = false, nullable = false)
	private Integer totalRating;
	
	@Column(name = "RA_NOR", unique = false, nullable = false)
	private Integer numberOfRatings;
	
	@ManyToOne
	@JoinColumn(name = "RE_ID", referencedColumnName = "RE_ID", nullable = false)
	private Restaurant restaurant;
	
	@ManyToOne
	@JoinColumn(name = "US_ID", referencedColumnName = "US_ID", nullable = false)
	private Guest guest;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(Integer totalRating) {
		this.totalRating = totalRating;
	}

	public Integer getNumberOfRatings() {
		return numberOfRatings;
	}

	public void setNumberOfRatings(Integer numberOfRatings) {
		this.numberOfRatings = numberOfRatings;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}
	
	
}
