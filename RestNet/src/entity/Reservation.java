package entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.openjpa.persistence.jdbc.VersionColumn;

@XmlRootElement
@Entity
@Table(name="RESERVATION")
@VersionColumn
public class Reservation implements Serializable {
	private static final long serialVersionUID = -6769755034894760110L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "RV_ID", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "RV_DATE1", unique = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date1;
	
	@Column(name = "RV_DATE2", unique = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date2;
	
	@ManyToOne
	@JoinColumn(name = "RE_ID", referencedColumnName = "RE_ID", nullable = false)
	private Restaurant restaurant;
	
	@XmlTransient
	@ManyToOne
	@JoinColumn(name = "US_ID", referencedColumnName = "US_ID", nullable = false)
	private Guest guest;
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy="reservation")
	@XmlTransient
	private Set<Invitation> invitations = new HashSet<Invitation>();
	
	public Reservation() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate1() {
		return date1;
	}
	
	public void setDate1(Date date1) {
		this.date1 = date1;
	}
	public Date getDate2() {
		return date2;
	}
	public void setDate2(Date date2) {
		this.date2 = date2;
	}
	

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	@XmlTransient
	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}

	@XmlTransient
	public Set<Invitation> getInvitations() {
		return invitations;
	}

	public void setInvitations(Set<Invitation> invitations) {
		this.invitations = invitations;
	}
}
