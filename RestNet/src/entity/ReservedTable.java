package entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.openjpa.persistence.jdbc.VersionColumn;

@Entity
@NamedQuery(name = "isTableReserved", query = "SELECT rt FROM ReservedTable rt WHERE rt.table.id = :t AND NOT(rt.reservation.date1 >= :date2 OR rt.reservation.date2 <= :date1)")
@Table(name="RESERVED_TABLE")
@VersionColumn
public class ReservedTable implements Serializable {
	private static final long serialVersionUID = -7542076723742426593L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "RT_ID", unique = true, nullable = false)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "TA_ID", referencedColumnName = "TA_ID", nullable = false)
	private TableX table;
	
	@ManyToOne
	@JoinColumn(name = "RV_ID", referencedColumnName = "RV_ID", nullable = false)
	private Reservation reservation;
	
	public ReservedTable() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Reservation getReservation() {
		return reservation;
	}
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	public TableX getTable() {
		return table;
	}
	public void setTable(TableX table) {
		this.table = table;
	}
}
