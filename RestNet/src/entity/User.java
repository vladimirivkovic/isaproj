package entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.openjpa.persistence.jdbc.VersionColumn;

@Entity
@Table(name="USER")
@VersionColumn
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="US_ROLE", discriminatorType=DiscriminatorType.STRING)
@NamedQueries({
	@NamedQuery(name = "findUserByUsernameAndPassword", query = "SELECT u FROM User u WHERE u.username = :username AND u.password = :password"),
	@NamedQuery(name = "findUserByToken", query = "SELECT u FROM User u WHERE u.token = :tokenValue AND u.token is not null")
})
public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2744639820726890521L;
	
	public static final String ADMIN = "A";
	public static final String MANAGER = "M";
	public static final String GUEST = "G";
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "US_ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "US_USERNAME", unique = true, nullable = false)
	private String username;
	
	@Column(name = "US_PASSWORD", unique = false, nullable = false)
	private String password;
	
	@Column(name = "US_ROLE", unique = false, nullable = false)
	private String role;
	
	@Column(name = "US_TOKEN", unique = true, nullable = true)
	private String token;
	
	public User() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String name) {
		this.username = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String type) {
		this.role = type;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (obj instanceof User) {
			return this.getId().equals(((User) obj).getId());
		} else {
			return false;
		}	
	}
}
