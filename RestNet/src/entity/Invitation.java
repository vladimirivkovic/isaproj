package entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.openjpa.persistence.jdbc.VersionColumn;

@XmlRootElement
@Entity
@Table(name = "INVITATION")
@VersionColumn
public class Invitation implements Serializable {
	private static final long serialVersionUID = -3445468539533949541L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "IN_ID", unique = true, nullable = false)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "RV_ID", referencedColumnName = "RV_ID", nullable = false)
	private Reservation reservation;

	@ManyToOne
	@JoinColumn(name = "US_ID", referencedColumnName = "US_ID", nullable = false)
	private Guest guest;

	@Column(name = "IN_RES", unique = false, nullable = false)
	private Boolean responded;

	public Invitation() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}

	public Boolean getResponded() {
		return responded;
	}

	public void setResponded(Boolean responded) {
		this.responded = responded;
	}

}
