package entity;


import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.openjpa.persistence.jdbc.VersionColumn;

@XmlRootElement
@Entity
@Table(name="FRIENDSHIP")
@VersionColumn
@NamedQuery(name = "areFriends", query = "SELECT f FROM Friendship f WHERE f.firstFriend = :first AND f.secondFriend = :second")
public class Friendship implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "FR_ID", unique = true, nullable = false)
	private Integer id;
   
	@ManyToOne
	@JoinColumn(name = "US_ID1", referencedColumnName = "US_ID", nullable = false)
	private Guest firstFriend;
	
	@ManyToOne
	@JoinColumn(name = "US_ID2", referencedColumnName = "US_ID", nullable = false)
	private Guest secondFriend;
	
	public Friendship() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Guest getFirstFriend() {
		return firstFriend;
	}

	public void setFirstFriend(Guest firstFriend) {
		this.firstFriend = firstFriend;
	}

	public Guest getSecondFriend() {
		return secondFriend;
	}

	public void setSecondFriend(Guest secondFriend) {
		this.secondFriend = secondFriend;
	}
	
	
}
