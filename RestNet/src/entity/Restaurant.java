package entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.openjpa.persistence.jdbc.VersionColumn;

@XmlRootElement
@Entity
@Table(name="RESTAURANT")
@VersionColumn
public class Restaurant implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2907024083130706472L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "RE_ID", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "RE_NAME", unique = false, nullable = false)
	private String name;
	
	@Column(name = "RE_TYPE", unique = false, nullable = false)
	private String type;
	
	@Column(name = "RE_LONG", unique = false, nullable = false)
	private Double longitude;
	
	@Column(name = "RE_LAT", unique = false, nullable = false)
	private Double latitude;
	
	@Column(name = "RE_RATING", unique = false, nullable = false)
	private Integer totalRating;
	
	@Column(name = "RE_NOR", unique = false, nullable = false)
	private Integer numberOfRatings;
	
	@Column(name = "RE_CONF", unique = false, nullable = false)
	private Boolean configured;
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy="restaurant")
	private Set<Food> menu = new HashSet<Food>();
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy="restaurant")
	private Set<TableX> tables = new HashSet<TableX>();
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy="restaurant")
	private Set<Reservation> reservation = new HashSet<Reservation>();
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy="restaurant")
	private Set<Rating> ratings = new HashSet<Rating>();
	
	public Restaurant() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Integer getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(Integer totalRating) {
		this.totalRating = totalRating;
	}

	public Integer getNumberOfRatings() {
		return numberOfRatings;
	}

	public void setNumberOfRatings(Integer numberOfRatings) {
		this.numberOfRatings = numberOfRatings;
	}

	public Set<Food> getMenu() {
		return menu;
	}

	public void setMenu(Set<Food> menu) {
		this.menu = menu;
	}

	public Set<TableX> getTables() {
		return tables;
	}

	public void setTables(Set<TableX> tables) {
		this.tables = tables;
	}

	public Set<Reservation> getReservation() {
		return reservation;
	}

	public void setReservation(Set<Reservation> reservation) {
		this.reservation = reservation;
	}

	public Set<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}
	
	public Boolean getConfigured() {
		return configured;
	}
	
	public void setConfigured(Boolean configured) {
		this.configured = configured;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj instanceof Restaurant) {
			if (((Restaurant) obj).getId() == null) {
				return false;
			} else {
				return id.equals(((Restaurant) obj).getId());
			}
		} else {
			return false;
		}
	}
}
