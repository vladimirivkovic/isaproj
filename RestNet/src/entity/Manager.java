package entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@DiscriminatorValue("M")
public class Manager extends User {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8689897607852393104L;
	
	@ManyToOne
	@JoinColumn(name = "RE_ID", referencedColumnName = "RE_ID", nullable = false)
	private Restaurant restaurant;
	
	public Manager() {
		// TODO Auto-generated constructor stub
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
	
	
}
