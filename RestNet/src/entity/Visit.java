package entity;


import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.openjpa.persistence.jdbc.VersionColumn;

/**
 * Entity implementation class for Entity: Visit
 *
 */

@XmlRootElement
@Entity
@Table(name="VISIT")
@VersionColumn
@NamedQueries({
@NamedQuery(name = "findVisitsFor", query = "SELECT v FROM Visit v WHERE v.reservation.date2 < CURRENT_DATE AND v.guest = :g"),
@NamedQuery(name = "findFriendsForVisit", query = "SELECT v.guest FROM Visit v WHERE v.reservation.date2 < CURRENT_DATE AND v.reservation = :res"),
})
public class Visit implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5644333510954331656L;

	public Visit() {
		super();
	}
	
	public Visit(Invitation inv) {
		rated = false;
		rating = null;
		reservation = inv.getReservation();
		guest = inv.getGuest();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "VI_ID", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "VI_RATING", unique = false, nullable = true)
	private Integer rating;
	
	@Column(name = "VI_RATED", unique = false, nullable = false)
	private Boolean rated;
	
	@ManyToOne
	@JoinColumn(name = "RV_ID", referencedColumnName = "RV_ID", nullable = false, unique = true)
	private Reservation reservation;
	
	@ManyToOne
	@JoinColumn(name = "US_ID", referencedColumnName = "US_ID", nullable = false)
	private Guest guest;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
	public Guest getGuest() {
		return guest;
	}
	public void setGuest(Guest guest) {
		this.guest = guest;
	}
	public Boolean getRated() {
		return rated;
	}
	public void setRated(Boolean rated) {
		this.rated = rated;
	}
	public Reservation getReservation() {
		return reservation;
	}
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	
}
