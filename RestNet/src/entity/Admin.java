package entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@DiscriminatorValue("A")
public class Admin extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7249946877130664106L;
}
